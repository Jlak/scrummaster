﻿/*
    Last Updated:13/04/2016
*/
var autocompleteCombo = {
    lastZIndex: 99,

    unbindCombo: function (controlID) {
        $("div." + controlID).remove();
    },

    initialize: function (controlID, dependentControlID,applyFullWidth) {
        this.createSearchDiv(controlID,applyFullWidth);
        this.prepComboOptions(controlID);

        $("#" + controlID).css({
            "visibility": "hidden",
            "max-width": "2px"
        });

        if (dependentControlID != null) {
            $("#" + controlID).attr("dependent-controlid", dependentControlID);
        }

        var currentValue = $("#" + controlID).val();

        if (currentValue != null && currentValue.length > 0) {
            autocompleteCombo.selectValue(controlID, currentValue, true);
        }
    },

    createSearchDiv: function (controlID,applyFullWidth) {
        autocompleteCombo.unbindCombo(controlID);

        var div = $("<div class='" + controlID + "'></div>");

        div.css({
            //"position": "absolute",
            "z-index": (autocompleteCombo.lastZIndex--).toString(),
            "height": "5px",
            "white-space": "nowrap"
        });

        div.on("focusout", function () {
            if (!$(this).is(":hover")) {
                $(this).find("ul").empty();
                $(this).find("input[type='text']").val($("#" + controlID + " option:selected").text());
            }
        });

        var textBox = $("<input type='text' placeholder='type to search...' class='auto-combo form-control' parent-id='" + controlID + "' />");

        textBox.on("keyup", function () {
            autocompleteCombo.searchText(this, false);
        });

        div.append(textBox);

        var button = $("<input type='button' parent-id='" + controlID + "' value='▼'/>");

        button.addClass("auto-combo-button");

        button.on("click", function () {
            autocompleteCombo.searchText(this, true);
        });

        div.append(button);

        var list = $("<ul></ul>");

        list.addClass("auto-combo-list");

        div.append(list);

        $("#" + controlID).before(div);

        if (applyFullWidth) {
            textBox.addClass("full-width");
        }

        autocompleteCombo.updateListSize(textBox, list);
    },

    //Resize List according to SearchBox
    updateListSize: function (textBox, list) {
        list.width(textBox.width() + 25);
        list.css("max-width", (textBox.outerWidth()) + "px");
    },

    prepComboOptions: function (controlID) {
        $("#" + controlID + " option").each(function (i, d) {
            $(this).attr("lower-text", $(d).text().toLowerCase());
        });
    },

    searchText: function (caller, showAllResults) {
        var combo = $(caller).closest("div").find("input[type='text']");

        var currentValue = combo.val();

        var parentID = combo.attr("parent-id");

        var selector = "#" + parentID + " option";

        var resultsList = $("div." + parentID + " ul");

        if (!showAllResults) {
            var searchTerm = currentValue.toLowerCase();

            selector += "[lower-text*='" + autocompleteCombo.sanitizeSearchValue(searchTerm) + "']";
        }
        else if (resultsList.find("li").length > 0) {//Already showing
            resultsList.empty();
            return;
        }

        resultsList.empty();

        $(selector).each(function (i, d) {

            var result = $("<li>" + $(d).text() + "</li>");

            result.attr("parent-id", parentID);
            result.attr("my-value", $(d).attr("value"));

            result.on("mousedown", function () {
                autocompleteCombo.selectValue(parentID, $(d).attr("value"));
            });

            resultsList.append(result);
        });

        autocompleteCombo.updateListSize(combo, resultsList);
    },

    selectValue: function (parentID, value, doNotTriggerChange) {
        $("#" + parentID).val(value);

        var dependentControlID = $("#" + parentID).attr("dependent-controlid");

        if (dependentControlID != null && doNotTriggerChange == null) {
            autocompleteCombo.selectValue(dependentControlID, "", true);
        }

        if (doNotTriggerChange == null) {
            $("#" + parentID).trigger("change");
        }

        $("div." + parentID + " input[type='text']").val($("#" + parentID + " option[value='" + autocompleteCombo.sanitizeSearchValue(value) + "']").text());

        var resultsList = $("div." + parentID + " ul");

        resultsList.empty();
    },

    emptyComboAndDependants: function (controlID,deleteFirstOption) {
        var combo = $("#" + controlID);
        combo.val("");

        var dependentControlID = combo.attr("dependent-controlid");
        $("div." + controlID + " input[type='text']").val($("#" + controlID + " option[value='']").text());

        var resultsList = $("div." + controlID + " ul");

        resultsList.empty();

        if (deleteFirstOption) {
            combo.find("option").remove();
        }
        else {
            combo.find("option:not(:first-of-type)").remove();
        }

        if (dependentControlID != null) {
            autocompleteCombo.emptyComboAndDependants(dependentControlID);
        }
    },

    //escape all single quotes
    sanitizeOptionValue: function (value) {
        return (value == null ? "" : value.toString()).replace(/[']/g, "&#39;").replace(/"/g, "&quot;");
    },

    //escape all single quotes for JQuery search
    sanitizeSearchValue: function (value) {
        return (value == null ? "" : value.toString()).replace(/[']/g, "\'").replace(/"/g, "\"");
}
};