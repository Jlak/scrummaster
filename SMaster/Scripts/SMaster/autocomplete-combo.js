﻿var autocompleteCombo = {
    lastZIndex: 99,

    unbindCombo: function (controlID) {
        $("div." + controlID).remove();
    },

    initialize: function (controlID, dependentControlID) {
        this.createSearchDiv(controlID);
        this.prepComboOptions(controlID);
        $("#" + controlID).css({
            "visibility": "hidden",
            "max-width": "2px"
        });

        if (dependentControlID != null) {
            $("#" + controlID).attr("dependent-controlid", dependentControlID);
        }

        var currentValue = $("#" + controlID).val();

        if (currentValue != null && currentValue.length > 0) {
            autocompleteCombo.selectValue(controlID, currentValue, true);
        }
    },
    createSearchDiv: function (controlID) {
        autocompleteCombo.unbindCombo(controlID);

        var div = $("<div class='" + controlID + "'></div>");

        div.css({
            //"position": "absolute",
            "z-index": (autocompleteCombo.lastZIndex--).toString(),
            "height": "5px",
            "white-space": "nowrap"
        });

        div.on("focusout", function () {
            if (!$(this).is(":hover")) {
                $(this).find("ul").empty();
                $(this).find("input[type='text']").val($("#" + controlID + " option:selected").text());
            }
        });

        var textBox = $("<input type='text' placeholder='type to search...' class='auto-combo form-control' parent-id='" + controlID + "' />");

        textBox.on("keyup", function () {
            autocompleteCombo.searchText(this, false);
        });

        div.append(textBox);

        var button = $("<input type='button' parent-id='" + controlID + "' value='▼'/>");

        button.css({
            "border-top-right-radius": "4px",
            "border-bottom-right-radius": "4px",
            "border": "1px solid #DDD",
            "padding": "2px 5px 2px 5px",
            "cursor": "pointer",
            "background-color": "#EEE",
            "margin-left": "-1px"
        });

        button.on("mouseenter", function () {
            $(this).css({
                "border": "1px solid #CCC",
                "background-color": "#DDD"
            });
        });

        button.on("mouseleave", function () {
            $(this).css({
                "border": "1px solid #DDD",
                "background-color": "#EEE"
            });
        });

        button.on("click", function () {
            autocompleteCombo.searchText(this, true);
        });

        div.append(button);

        var list = $("<ul></ul>");

        list.css({
            "position": "absolute",
            "z-index": 4,
            "max-height": "200px",
            "overflow": "auto",
            "border-radius": "3px",
            "list-style": "none",
            "margin-left": "-40px",
            "margin-top": "0px"
        });

        div.append(list);

        $("#" + controlID).before(div);
    },

    prepComboOptions: function (controlID) {
        $("#" + controlID + " option").each(function (i, d) {
            $(this).attr("lower-text", $(d).text().toLowerCase());
        });
    },

    searchText: function (caller, showAllResults) {
        var combo = $(caller).closest("div").find("input[type='text']");

        var currentValue = combo.val();

        var parentID = combo.attr("parent-id");

        var selector = "#" + parentID + " option";

        var resultsList = $("div." + parentID + " ul");

        if (!showAllResults) {
            var searchTerm = currentValue.toLowerCase();

            selector += "[lower-text*='" + searchTerm + "']";
        }
        else if (resultsList.find("li").length > 0) {//Already showing
            resultsList.empty();
            return;
        }

        resultsList.empty();

        $(selector).each(function (i, d) {

            var result = $("<li>" + $(d).text() + "</li>");

            result.attr("parent-id", parentID);
            result.attr("my-value", $(d).attr("value"));

            result.on("mousedown", function () {
                autocompleteCombo.selectValue(parentID, $(d).attr("value"));
            });

            result.css({
                "padding": "5px",
                "background-color": "#EEE",
                "cursor": "pointer",
                "border-bottom": "1px solid #DDD",
                "border-left": "1px solid #DDD",
                "border-right": "1px solid #DDD"
            });

            result.on("mouseenter", function () {
                $(this).css("background-color", "#FFF");
                $(this).focus();
            });

            result.on("mouseleave", function () {
                $(this).css("background-color", "#EEE");
            });

            resultsList.append(result);
        });

        resultsList.find("option:first-of-type").css({
            "border-top": "1px solid #EEE",
            "border-top-left-radius": "4px",
            "border-top-right-radius": "4px"
        });
        resultsList.find("option:last-of-type").css({
            "border-bottom": "1px solid #EEE",
            "border-bottom-left-radius": "4px",
            "border-bottom-right-radius": "4px"
        });
    },

    selectValue: function (parentID, value, doNotTriggerChange) {
        $("#" + parentID).val(value);

        var dependentControlID = $("#" + parentID).attr("dependent-controlid");

        if (dependentControlID != null && doNotTriggerChange == null) {
            autocompleteCombo.selectValue(dependentControlID, "", true);
        }

        if (doNotTriggerChange == null) {
            $("#" + parentID).trigger("change");
        }

        $("div." + parentID + " input[type='text']").val($("#" + parentID + " option[value='" + value + "']").text());

        var resultsList = $("div." + parentID + " ul");

        resultsList.empty();
    }
};