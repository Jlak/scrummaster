﻿var helper = {
    /*
    VERSION: 1.00
    LAST MODIFIED: 2015-11-04
    */

    errorHandler: function (jqXHR, textStatus, error) {
        try {
            hideBusyDiv();
        }
        catch (e) { }
        alert("Request Status: " + textStatus + "\n\nDetails: " + error);
    }
};