﻿helper.controls = {
    SERVER_DATE_TODAY: null,
    SERVER_DATE_TASK_CREATED: null,
    ACTIVE_MENU_ID: null,
    MENU_ID_TO_ACTIVATE: null,
    CSS_CLASSES_TO_REMOVE: "",
    REQUIRE_COMMAS_CLASS: "input.require-commas",

    initialize: function (enableDatePicker) {
        this.formatControls();

        $("body").on("keyup change", this.REQUIRE_COMMAS_CLASS, this.bindCommasForMoneyField);
    },

    //Datepicker initialization
    initializeDatePickers: function () {
        var currentNamespace = helper.controls;
        $(".datepicker").each(function (i, d) {
            var dateBox = $(d);
            var configParams = {
                format: "YYYY-MM-DD",
                showClear: true,
                showTodayButton: true,
                showClose: true,
                widgetPositioning: { vertical: "bottom" }
            };

            if (dateBox.hasClass("present")) {
                configParams.maxDate = currentNamespace.SERVER_DATE_TODAY;
            }
            else if (dateBox.hasClass("no-past")) {
                configParams.minDate = currentNamespace.SERVER_DATE_TODAY;
            }
            else if (dateBox.hasClass("customstartDate")) {
                configParams.minDate = currentNamespace.FINANCE_PERIOD_DATE;
                configParams.maxDate = currentNamespace.FINANCE_PERIOD_DATE;
            }

            if (dateBox.hasClass("show-time")) {
                configParams.format = "YYYY-MM-DD HH:mm";
                configParams.sideBySide = true;
            }

            var currentValue = dateBox.val();

            dateBox.datetimepicker(configParams);

            dateBox.val(currentValue);
        });
    },

    postInitializationLogic: function () { },

    removeRowPreLogic: function () { },

    removeRowPostLogic: function () { },

    formatControls: function () {

        var currentNamespace = helper.controls;

        if (currentNamespace.MENU_ID_TO_ACTIVATE != null) {
            currentNamespace.ACTIVE_MENU_ID = currentNamespace.MENU_ID_TO_ACTIVATE;
        }

        if (currentNamespace.ACTIVE_MENU_ID != null) {
            $("#" + currentNamespace.ACTIVE_MENU_ID).addClass("active");
        }

        if (currentNamespace.CSS_CLASSES_TO_REMOVE.length > 0) {
            $(currentNamespace.CSS_CLASSES_TO_REMOVE).remove();
        }

        $("input[type='text'],input[type='password'],textarea,select").addClass("form-control");
        helper.controls.initializeDatePickers();

        $(".has-tooltip").attr("data-toggle", "tooltip").tooltip();

        $(".remove-row").click(function () {
            helper.controls.removeRowPreLogic();

            $(this).closest("tr").remove();
            helper.controls.watchedcollections.update();

            helper.controls.removeRowPostLogic();
        });

        this.watchedcollections.update();

        $("input.require-commas").trigger("change");

        //Rebind form validation when watching collections
        var closestForm = $(this.watchedcollections.CLASS_NAME + ":first-of-type").closest("form");
        if (closestForm != null) {
            this.rebindFormValidation(closestForm);
        }

        ////Fix headers
        //$('.table-header-fixed').each(function (i, d) {
        //    $table = $(d);
        //    $table.floatThead({
        //        scrollContainer: function ($table) {
        //            return $table.closest('.table-responsive');
        //        }
        //    });
        //});
    },

    confirmFormSubmission: function (controlId, confirmationMessage) {
        $("#" + controlId).closest("form").on("submit", function () {
            if (!$(this).valid()) {
                alert("Please fill all required fields!");
                return false;
            }
            else if (!helper.customvalidator.isFormValid(this)) {
                alert("Please resolve all validation errors!");
                return false;
            }

            if (confirmationMessage == null) {
                confirmationMessage = "Do you confirm saving these details?";
            }

            return confirm(confirmationMessage);
        });
    },

    rebindFormValidation: function (form) {
        form.removeData("validator");
        form.removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse(form);
    },

    bindCommasForMoneyField: function (event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) {
            event.preventDefault();
        }

        $(this).val(function (index, value) {
            value = value.replace(/,/g, ''); // remove commas from existing input
            return helper.controls.numberWithCommas(value); // add commas back in
        });
    },

    numberWithCommas: function (x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    addCommasToInput: function () {
        var input = this;
        var val = $(input).val();

        val = val.replace(/,/g, "");
        val = val.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $(input).val(val);
    },

    addCommas: function (val) {
        return val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    },

    removeCommas: function (val) {
        return val.replace(/,/g, "");
    },

    removeCommasFromInputs: function () {
        var parentClass = this;
        $(this.REQUIRE_COMMAS_CLASS).each(function (i, d) {
            $(d).val(parentClass.removeCommas($(d).val()));
        });
    },

    getNumericValue: function (text) {
        var result = 0;

        try {
            result = this.removeCommas(text) * 1;

            if (isNaN(result)) {
                result = 0;
            }
        }
        catch (e) { }

        return result;
    },

    triggerAddCommas: function () {
        $(this.REQUIRE_COMMAS_CLASS).change();
    },

    getFormValues: function (form) {
        this.removeCommasFromInputs();
        var values = $(form).serialize();
        this.triggerAddCommas();
        return values;
    }
};

helper.controls.watchedcollections = {
    /*
    VERSION: 1.12
    */

    CLASS_NAME: ".watched-collection",
    NAME_ATTRIBUTE: "collection-name",
    INDEX_ATTRIBUTE: "collection-index",

    //PUBLIC: Call this to rename controls
    update: function () {
        this.rebindCollectionNames();
    },

    //INTERNAL
    rebindCollectionNames: function () {
        var watchedCollectionNames = [];

        var watchedCollections = this.getWatchedCollections();

        this.recursivelyRenameInputs(this, watchedCollections);
    },

    //INTERNAL
    getWatchedCollections: function () {
        var watchedCollections = [];

        var watchedCollectionNames = [];

        var parentClass = this;

        //Isolate distinct collections being watched
        $(parentClass.CLASS_NAME).each(function (i, d) {
            watchedCollectionNames.push($(d).attr(parentClass.NAME_ATTRIBUTE));
        });
        watchedCollectionNames = this.distinct(watchedCollectionNames);

        //For each collection
        $(watchedCollectionNames).each(function (i, d) {
            //Isolate distinct control names
            var formInputNames = [];

            $(parentClass.CLASS_NAME + "[" + parentClass.NAME_ATTRIBUTE + "='" + d + "']:eq(0)")
                .find("input[type!='button'][type!='submit']:not(.auto-combo),textarea,select").each(function (j, w) {
                    var input = $(w);

                    if (input.closest(parentClass.CLASS_NAME).attr(parentClass.NAME_ATTRIBUTE) == d) {//Must be child of current watchedCollectionName
                        var inputName = input.attr("name");
                        formInputNames.push(inputName.substr(inputName.lastIndexOf(".") + 1, 100));
                    }
                });
            formInputNames = parentClass.distinct(formInputNames);

            formInputNames = parentClass.sortByLengthDescending(formInputNames);

            watchedCollections.push({
                collectionName: d,
                inputNames: formInputNames,
                level: d.split('.').length
            });
        });

        return watchedCollections;
    },

    //INTERNAL
    extractWatchedCollections: function (watchedCollections, level, collectionNamePrefix) {
        var result = [];

        $(watchedCollections).each(function (i, d) {
            if (d.level == level) {
                if (collectionNamePrefix != null) {
                    if (d.collectionName.startsWith(collectionNamePrefix)) {
                        result.push(d);
                    }
                }
                else {
                    result.push(d);
                }
            }
        });

        return result;
    },

    //INTERNAL
    recursivelyRenameInputs: function (parentClass, watchedCollections, parentContainer, parentCollection, parentIndexedCollectionNamePrefix, currentLevel) {
        var currentIndexedCollectionNamePrefix = "";

        if (parentIndexedCollectionNamePrefix != null) {
            currentIndexedCollectionNamePrefix = parentIndexedCollectionNamePrefix + ".";
        }

        if (parentCollection != null) {
            //For each row
            $(parentContainer).find(parentClass.CLASS_NAME + "[" + parentClass.NAME_ATTRIBUTE + "='" + parentCollection.collectionName + "']")
            .each(function (j, row) {
                var currentRowCollectionName = currentIndexedCollectionNamePrefix
                    + parentCollection.collectionName.substr(parentCollection.collectionName.lastIndexOf(".") + 1)
                    + "[" + j + "]";

                //For each input
                $(parentCollection.inputNames).each(function (k, inputName) {

                    var selector = "input[name='" + inputName + "'],textarea[name='" + inputName + "'],select[name='" + inputName + "']"
                    + ",input[name$='." + inputName + "'],textarea[name$='." + inputName + "'],select[name$='." + inputName + "']";

                    //For each row input
                    $(row).find(selector)
                        .each(function (k, element) {
                            var input = $(element);

                            var itemContainer = input.closest(parentClass.CLASS_NAME);

                            if (itemContainer.attr(parentClass.NAME_ATTRIBUTE) == parentCollection.collectionName) {//Must be child of current collection
                                input.attr("name", currentRowCollectionName + "." + inputName);
                                var newID = currentRowCollectionName + "." + inputName;
                                newID = newID.replace(".", "_");
                                newID = newID.replace("[", "_");
                                newID = newID.replace("]", "_");
                                input.attr("id", newID);
                                //rebind-validation
                                input.next(".field-validation-valid[data-valmsg-for='" + inputName + "'],.field-validation-valid[data-valmsg-for$='." + inputName + "']")
                                .attr("data-valmsg-for", newID);
                            }
                        });
                });

                var childCollections = parentClass.extractWatchedCollections(watchedCollections, currentLevel + 1, parentCollection.collectionName);

                if (childCollections.length > 0) {//Only recursively call when child collections are found
                    $(childCollections).each(function (i, collectionItem) {
                        parentClass.recursivelyRenameInputs(parentClass, watchedCollections, row, collectionItem, currentRowCollectionName, currentLevel + 1);
                    });
                }
            });
        }
        else {//Start the loop
            $(parentClass.extractWatchedCollections(watchedCollections, 1)).each(function (i, collectionItem) {
                parentClass.recursivelyRenameInputs(parentClass, watchedCollections, document.body, collectionItem, null, 1);
            });
        }
    },

    //INTERNAL
    distinct: function (array) {
        var result = [];

        $(array).each(function (i, d) {
            if (result.indexOf(d) < 0) {
                result.push(d);
            }
        });

        return result;
    },

    sortByLengthDescending: function (array) {

        for (var i = 0; i < array.length - 1; i++) {
            for (var j = 1; j < array.length; j++) {
                if (array[j].length > array[i].length) {
                    var temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                }
            }
        }

        return array;
    }
};