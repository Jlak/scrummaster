﻿//Renders and manages grids
helper.gridmenu = {
    /*
    VERSION: 1.0
    */
    initialize: function (parentId,numberOfTilesPerRow) {
        var gridContainer = $("#"+parentId);

        var tileCount = gridContainer.find(".gridmenu-block").length;

        //fill in blank tiles to complete rows
        while (tileCount % numberOfTilesPerRow !=0 ) {
            var newTile = $("<div></div>");
            newTile.addClass("gridmenu-block");

            var innerCard = $("<div></div>");
            innerCard.addClass("gridmenu-card");

            newTile.append(innerCard);

            gridContainer.append(newTile);
            tileCount++;
        }

        var numberOfRows = tileCount / numberOfTilesPerRow;

        var tileWidth = 220, iconFontSize = 46;

        //resize tiles and icon fonts depending on number of rows to avoid over-scrolling
        switch (numberOfRows) {
            case 6:
            case 5:
            case 4:
                tileWidth = 140;
                iconFontSize = 35;
                break;
            case 3:
                tileWidth = 190;
                iconFontSize = 43;
                break;
            default:
                tileWidth = 220;
                iconFontSize = 46;
        }

        //set the grid container max-width to regulate rows
        var tileCountainerMaxWidth = (numberOfTilesPerRow * tileWidth) + 50;

        gridContainer.css("max-width", tileCountainerMaxWidth + "px");

        gridContainer.find(".gridmenu-block .fa").css("font-size", iconFontSize + "px");

        gridContainer.find(".gridmenu-card figure").css("line-height", tileWidth + "px");

        gridContainer.find(".gridmenu-block").css({
            height: tileWidth + "px",
            width: tileWidth + "px"
        }).hover(function () {
            $(this).find(".gridmenu-card").addClass("flipped");
        }, function () {
            $(this).find(".gridmenu-card").removeClass("flipped");
        });
    }
};