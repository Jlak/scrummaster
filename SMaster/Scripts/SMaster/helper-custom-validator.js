﻿helper.customvalidator = {
    /*
    Custom validation Logic
    */
    VALIDATION_FAILURE_CLASS: "custom-validation-failed",
    VALIDATION_MESSAGE_CLASS: "custom-validation-message",

    //validation classes
    VALIDATE_GREATER_THAN_ZERO: ".validate-greater-than-zero",
    VALIDATE_GREATER_THAN_OR_EQUAL_ZERO: ".validate-greater-than-or-equal-zero",
    VALIDATE_REQUIRED: ".validate-required",
    VALIDATE_MAXIMUM: ".validate-maximum",

    //attributes
    ATTRIBUTE_MAXIMUM:"data-maximum",

    initialize: function () {
        //bind
        $("body").on("change keyup", this.VALIDATE_GREATER_THAN_ZERO, this.mustBeGreaterThanZero);
        $("body").on("change keyup", this.VALIDATE_GREATER_THAN_OR_EQUAL_ZERO, this.mustBeGreaterThanOrEqualZero);
        $("body").on("change keyup", this.VALIDATE_REQUIRED, this.validateRequired);
        $("body").on("change keyup", this.VALIDATE_MAXIMUM, this.validateMaximum);

        //trigger
        $(this.VALIDATE_GREATER_THAN_ZERO).change();
        $(this.VALIDATE_GREATER_THAN_OR_EQUAL_ZERO).change();
        $(this.VALIDATE_REQUIRED).change();
        $(this.VALIDATE_MAXIMUM).change();
    },

    validateRequired: function (event) {
        var control = event.target;

        helper.customvalidator.setValidationResult(control, $(control).val().length != 0, "Required!");
    },

    getControlNumericValue:function(control){
        var value = 0;

        try {
            value = helper.controls.removeCommas($(control).val()) * 1;
        }
        catch (e) { }

        return value;
    },

    validateMaximum: function (event) {
        var control = event.target;

        var value = helper.customvalidator.getControlNumericValue(control);

        var maximumText=$(control).attr(helper.customvalidator.ATTRIBUTE_MAXIMUM);
        var maximum = helper.controls.getNumericValue(maximumText);

        helper.customvalidator.setValidationResult(control, value <= maximum, "must be <= " + maximumText+"!");
    },

    mustBeGreaterThanZero: function (event) {
        var control = event.target;

        var value = helper.customvalidator.getControlNumericValue(control);

        helper.customvalidator.setValidationResult(control, !(isNaN(value) || value <= 0), "must be > 0!");
    },

    mustBeGreaterThanOrEqualZero: function (event) {
        var control = event.target;

        var value = helper.customvalidator.getControlNumericValue(control);

        helper.customvalidator.setValidationResult(control, !(isNaN(value) || value < 0), "must be >= 0!");
    },

    setValidationResult: function (control, validationPassed, errorMessage) {
        if (validationPassed) {
            $(control).removeClass(helper.customvalidator.VALIDATION_FAILURE_CLASS);
        }
        else {
            $(control).addClass(helper.customvalidator.VALIDATION_FAILURE_CLASS);
        }
        helper.customvalidator.renderErrorMessage(control, errorMessage, !validationPassed);
    },

    renderErrorMessage: function (control, errorMessage, showError) {
        var closestContainer = $(control).closest("div,span,td,th");

        closestContainer.find("." + helper.customvalidator.VALIDATION_MESSAGE_CLASS).remove();

        if (showError) {
            var errorContainer = $("<span></span>");
            errorContainer.addClass(helper.customvalidator.VALIDATION_MESSAGE_CLASS);
            errorContainer.addClass("error-message");
            errorContainer.text(errorMessage);

            $(control).after(errorContainer);
        }
    },

    isFormValid: function (callingForm) {
        return $(callingForm).find("." + helper.customvalidator.VALIDATION_FAILURE_CLASS).length == 0;
    }
};