﻿helper.accordion = {
    /*
    VERSION: 1.0
    */
    ACTIVE_LINK_ID: null,

    initialize: function () {
        $(".custom-accordion>li>a").append("<span class='fa fa-caret-down pull-right'></span>")

        $(".custom-accordion>li>a").click(function () {

            var link = $(this);

            var ul = link.next();
            var clone = ul.clone().css({ "height": "auto" }).appendTo("body");
            var height = ul.css("height") === "0px" ? ul[0].scrollHeight + "px" : "0px";

            clone.remove();
            ul.animate({ "height": height });

            var linkIcon = link.find(".fa");

            if (linkIcon.hasClass("fa-caret-down")) {
                linkIcon.removeClass("fa-caret-down");
                linkIcon.addClass("fa-caret-up");
                //collapse any opened section
                $(".custom-accordion a.expanded").click();
                link.addClass("expanded");
            }
            else {
                linkIcon.removeClass("fa-caret-up");
                linkIcon.addClass("fa-caret-down");
                link.removeClass("expanded");
            }

            return false;
        });

        if (this.ACTIVE_LINK_ID != null) {
            $("#" + this.ACTIVE_LINK_ID).click();
        }
    }
};
