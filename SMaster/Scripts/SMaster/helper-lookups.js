﻿helper.lookups = {
    /*
    VERSION: 1.0
    */
    loadCombo: function (caller, url, parameters, preserveFirstOption, closestParentSelector, callbackfunction) {
        var combo = $(caller);
        var parentClass = this;
        var targetCombo = null;

        var foundCombo = false;

        combo.closest(closestParentSelector).find("select.related").each(function (i, d) {
            var selectBox = $(d);

            if (foundCombo) {
                parentClass.clearCombo(selectBox, preserveFirstOption);
                if (targetCombo == null) {
                    targetCombo = selectBox;
                }
            }

            if (!foundCombo && selectBox.attr("id") == combo.attr("id")) {
                foundCombo = true;
            }
        });

        $.post(url, parameters, function (response) {
            $(Object.keys(response)).each(function (i, d) {
                targetCombo.append("<option value='" + response[d] + "'>" + d + "</option>");
            });
            callbackfunction();
        });
    },

    clearCombo: function (combo, preserveFirstOption) {
        combo.val('');
        if (preserveFirstOption) {
            combo.find("option").each(function (i, d) {
                if (i > 0) {
                    $(d).remove();
                }
            });
        }
        else {
            combo.find("option").empty();
        }
    }
};