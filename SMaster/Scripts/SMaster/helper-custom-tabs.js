﻿helper.customtabs = {
    /*
    VERSION: 1.0
    */
    initialize: function () {
        var customtabs = this;

        $(".custom-tab-container").each(function (i, d) {
            customtabs.configureTabs($(d));
        });
    },

    configureTabs: function (tabContainer) {
        var maxHeight = 100;

        var allTabs = tabContainer.find(".custom-tab");

        var tabButtons = [];

        allTabs.each(function (i, d) {
            var tab = $(d);

            tabButtons.push({
                title: tab.attr("title"),
                id: tab.attr("id")
            });

            var tabHeight = tab.height();

            if (tabHeight > maxHeight) {
                maxHeight = tabHeight;
            }

            //hide tab
            tab.addClass("behind");
        });

        allTabs.height(maxHeight + 100);

        //construct tab buttons
        var tabButtonContainer = $("<ul></ul>");

        $(tabButtons).each(function (i, d) {
            var item = $("<li>" + d.title + "</li>");
            item.attr("id", d.id);
            item.click(smaster.customtabs.showTab);

            tabButtonContainer.append(item);
        });

        tabContainer.prepend(tabButtonContainer);

        //show first tab
        tabContainer.find("ul>li:first-of-type").click();
    },

    showTab: function () {
        var currentTabButton = $(this);

        if (currentTabButton.hasClass("active")) {//if current tab is already showing, ignore
            return;
        }

        var tabContainer = currentTabButton.closest(".custom-tab-container");

        //hide previous tab
        tabContainer.find(".custom-tab.active").removeClass("active").addClass("behind");
        tabContainer.find("ul>li.active").removeClass("active");

        //show current tab
        currentTabButton.addClass("active");
        tabContainer.find(".custom-tab[id='" + currentTabButton.attr("id") + "']").addClass("active").removeClass("behind");
    }
};