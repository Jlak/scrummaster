﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMaster.ViewModels;
using SMaster.Services;
using SMaster.Models;
using SMaster.Helpers;
using SMaster.Entities;
using SMaster.ViewModels.Products;
using SMaster.Models.Setups;

namespace SMaster.Controllers
{
    public class ProjectController : Controller
    {
        // GET: Product
        private ProductService _productService;

        public ProjectController()
        {
            _productService = new ProductService();
        }
        
        public ActionResult Index(ProjectListModel model)
        {
            model.FilterProjects(_productService);

            return View(model);
        }
        
        public ActionResult View(int Id)
        {
            return View(_productService.GetProjects().Where(r => r.Id == Id).Single());
        }
        public ActionResult Details(int? Id)
        {
            var Data = (_productService.GetProjects().Where(r => r.Id == Id).SingleOrDefault() ?? new Project());

            return View(Data);
        }

        [HttpPost]
        public JsonResult Save(Project record)
        {
            return Json(new
            {
                Success = _productService.SaveProject(record),
                ErrorMessage = record.ErrorMessage
            });
        }

        public ActionResult Delete(int Id)
        {
            string ErrorMessage = null;

            _productService.Delete(Id, out ErrorMessage);

            return RedirectToAction("Index");
        }

        #region Partials

        [HttpPost]
        public PartialViewResult PartialClients()
        {
            return PartialView("Partials/_PartialClients", new ProjectClient());
        }

        #endregion
    }
}
