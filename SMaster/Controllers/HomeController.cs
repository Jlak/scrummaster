﻿using SMaster.Services.Sprints;
using SMaster.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers
{
    public class HomeController : Controller
    {
        private SprintPlanService _sprintPlanService;
        public HomeController()
        {
            _sprintPlanService = new SprintPlanService();
        }
        public ActionResult Index(DashboardListModel model)
        {
            model.LoadData(_sprintPlanService);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            return View("Error");
        }

        public JsonResult LevelOfEffort(DashboardListModel model)
        {
            model.StartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            model.EndDate = DateTime.Today;
            model.LoadUsersLevelOfEffort(_sprintPlanService);

            return Json(model.Effort, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TopProject(DashboardListModel model)
        {
            model.StartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            model.EndDate = DateTime.Today;
            model.LoadTopProject(_sprintPlanService);

            return Json(model.TopProject, JsonRequestBehavior.AllowGet);
        }
    }
}