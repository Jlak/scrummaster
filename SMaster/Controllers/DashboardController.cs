﻿using SMaster.Helpers;
using SMaster.Services;
using SMaster.Services.Sprints;
using SMaster.ViewModels.Dashboard;
using SMaster.ViewModels.Sprint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers
{
    public class DashboardController : Controller
    {
        private SprintPlanService _sprintPlanService;
        private ProductService _productService;
        private NotificationService _notificationService;
        public DashboardController()
        {
            _sprintPlanService = new SprintPlanService();
            _productService = new ProductService();
            _notificationService = new NotificationService();
        }

        // GET: Dashboard
        public ActionResult Index(DashboardListModel model)
        {
            return View(model);
        }

        public ActionResult UnallocatedUsers(DashboardListModel model)
        {
            model.LoadUnallocatedUsers(_sprintPlanService);
            return View(model);
        }

        public ActionResult LevelOfEffortDetails(DashboardListModel model)
        {
            if (model.Export)
            {
                model.LoadWeeklyData(_sprintPlanService);

                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialTasks", model), "Weekly Report");
            }
            else
            {
                model.LoadWeeklyData(_sprintPlanService);

                return View(model);
            }
        }
        
        public ActionResult ProjectCost(DashboardListModel model)
        {
            if (model.Export)
            {
                model.LoadProjectCosting(_sprintPlanService);

                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialTasks", model), "Weekly Report");
            }
            else
            {
                model.LoadProjectCosting(_sprintPlanService);

                return View(model);
            }
        }

        public ActionResult ProjectPerUserCost(DashboardListModel model)
        {
            if (model.Export)
            {
                model.LoadProjectPerStaff(_sprintPlanService);

                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialTasks", model), "Weekly Report");
            }
            else
            {
                model.LoadProjectPerStaff(_sprintPlanService);

                return View(model);
            }
        }

        public ActionResult WeeklyReport(DashboardListModel model, int Success = 0)
        {
            if (model.Export)
            {
                model.LoadWeeklyData(_sprintPlanService);
                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialWeeklyReport", model), "Weekly Report");
            }
            else
            {
                model.LoadWeeklyData(_sprintPlanService);
                return View(model);
            }
        }

        public ActionResult SendReport(DateTime StartDate, DateTime EndDate)
        {
            _notificationService.PrepareNotification(StartDate, EndDate);
            return RedirectToAction("UsersLOEf");
        }

        public ActionResult UsersLOEf(DashboardListModel model)
        {
            if (model.Export)
            {
                model.LoadUsersLevelOfEffort(_sprintPlanService);

                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialLevelOfEffort", model), "Staff Level Of Effort");
            }
            else
            {
                model.LoadUsersLevelOfEffort(_sprintPlanService);

                return View(model);
            }
        }
    }
}

