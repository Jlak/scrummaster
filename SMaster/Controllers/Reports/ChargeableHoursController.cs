﻿using SMaster.Services;
using SMaster.Services.Sprints;
using SMaster.ViewModels.Dashboard;
using SMaster.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers.Reports
{
    public class ChargeableHoursController : Controller
    {

        private SprintPlanService _sprintPlanService;
        public ChargeableHoursController()
        {
            _sprintPlanService = new SprintPlanService();
        }

        // GET: ChargeableHours
        public ActionResult Index(BillableHoursViewModel model)
        {
            if (model.Export == Enums.ExportMode.EXCEL)
            {
                model.LoadChargeableHours(_sprintPlanService);

                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_partialChargeableHours", model), "Chargeable Hours");
            }
            else
            {
                model.LoadChargeableHours(_sprintPlanService);

                return View(model);
            }
        }

        public ActionResult PersonalInvoice(DashboardListModel model)
        {
            model.LoadFrequency(_sprintPlanService);
            if (model.Export)
            {
                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PersonalInvoice", model), "Personal Invoice");
            }
            return View(model);
        }

        public ActionResult ProjectExpenditure(BillableHoursViewModel model)
        {
            model.LoadExpenditure(_sprintPlanService);
            if (model.Export == Enums.ExportMode.EXCEL)
            {
                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialProjectHours", model), "Project Hours Worked");
            }
            else
            {
                return View(model);
            }
        }
    }
}