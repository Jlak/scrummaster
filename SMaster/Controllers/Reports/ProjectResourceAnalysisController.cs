﻿using SMaster.Services.Sprints;
using SMaster.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers.Reports
{
    public class ProjectResourceAnalysisController : Controller
    {
        private SprintPlanService _sprintPlanService;
        public ProjectResourceAnalysisController()
        {
            _sprintPlanService = new SprintPlanService();
        }

        // GET: ProjectResourceAnalysis
        public ActionResult Index(DateTime StartDate, DateTime EndDate, string UserId)
        {
            ProjectAnalysisListModel model = new ProjectAnalysisListModel();
            model.UserId = UserId;
            model.StartDate = StartDate;
            model.EndDate = EndDate;
            model.LoadProjectAnalysis(_sprintPlanService);

            return View(model);
        }
    }
}