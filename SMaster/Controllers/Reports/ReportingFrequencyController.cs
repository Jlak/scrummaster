﻿using SMaster.Services;
using SMaster.Services.Sprints;
using SMaster.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers.Reports
{
    public class ReportingFrequencyController : Controller
    {
        private SprintPlanService _sprintPlanService;
        public ReportingFrequencyController()
        {
            _sprintPlanService = new SprintPlanService();
        }

        // GET: ReportingFrequency
        public ActionResult Index(DashboardListModel model)
        {
            model.LoadFrequency(_sprintPlanService);
            if (model.Export)
            {
                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialFrequency", model), "Staff Reporting Frequency");
            }

            return View(model);
        }
    }
}