﻿using SMaster.Helpers;
using SMaster.Service.ControlPanel;
using SMaster.ViewModels.Users;
using System.Linq;
using System.Web.Mvc;

namespace SMaster.Controllers
{
    [Authorize(Roles = SMasterUserManager.SuperRole)]
    public class UsersController : Controller
    {
        // GET: ControlPanel/Users

        private ControlPanelService _controlpanelService;

        public UsersController()
        {
            _controlpanelService = new ControlPanelService();
        }

        public ActionResult Index(UsersListViewModel model)
        {
            model.LoadData(_controlpanelService);

            return View(model);
        }

        public ActionResult Create()
        {
            return View(new NewUserViewModel());
        }

        [HttpPost]
        public ActionResult Create(NewUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                string ErrorMessage = null;

                if (_controlpanelService.CreateUser(model.UserName, model.Password, model.FirstName, model.LastName, model.OtherName, model.Email, model.IsActive
                    , model.Phone, model.UserRole, model.DailyRate, out ErrorMessage))
                {
                    return RedirectToAction("Index", new { SearchTerm = model.UserName});
                }

                model.ErrorMessage = ErrorMessage;
            }
            else
            {
                model.ErrorMessage = "Please fill all required fields!";
            }

            return View(model);
        }
        

        public ActionResult Details(string Id, int Success = 0)
        {
            return View((from a in _controlpanelService.GetUsers().Where(r => r.Id == Id)
                         from b in a.Roles
                         join c in _controlpanelService.GetUserRoles() on b.RoleId equals c.Id into JoinResult
                         from d in JoinResult.DefaultIfEmpty()
                         select new EditUserViewModel
                         {
                             Email = a.Email,
                             FirstName = a.FirstName,
                             LastName = a.LastName,
                             OtherName = a.OtherName,
                             Phone = a.PhoneNumber,
                             Initials = a.Initials,
                             UserName = a.UserName,
                             UserRole = d.Name,
                             DailyRate = a.DailyRate,
                             Id = a.Id,
                             IsActive = a.IsActive,
                         }).Single());
        }

        [HttpPost]
        public JsonResult UpdateDetails(EditUserViewModel model)
        {
            string ErrorMessage = null;
            bool Success = false;

            if (ModelState.IsValid)
            {
                if (_controlpanelService.UpdateUser(model.Id, model.FirstName, model.LastName, model.OtherName, model.Email, model.Initials, model.IsActive
                    , model.Phone, model.UserRole,model.DailyRate, out ErrorMessage))
                {
                    Success = true;
                }
            }
            else
            {
                ErrorMessage = "Please fill all required fields!";
            }

            return Json(new
            {
                Success,
                ErrorMessage
            });
        }

        [HttpPost]
        public JsonResult ResetPassword(string Id, string NewPassword)
        {
            string ErrorMessage = null;

            return Json(new
            {
                Success = _controlpanelService.ResetUserPassword(Id, NewPassword, out ErrorMessage),
                ErrorMessage = ErrorMessage
            });
        }
    }
}