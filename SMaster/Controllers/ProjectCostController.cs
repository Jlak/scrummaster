﻿using SMaster.Services;
using SMaster.Services.Sprints;
using SMaster.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers
{
    public class ProjectCostController : Controller
    {
        private SprintPlanService _sprintPlanService;
        public ProjectCostController()
        {
            _sprintPlanService = new SprintPlanService();
        }
        // GET: ProjectCost
        public ActionResult Index(ProjectCostListModel model)
        {
            if (model.Export)
            {
                model.LoadExpenditure(_sprintPlanService);
                return UtilityService.ExportPartialToExcel(Response, PartialView("Partials/_PartialExpenditure", model), "Project Expenditure Report");
            }
            else
            {
                model.LoadExpenditure(_sprintPlanService);
                return View(model);
            }
        }

        [HttpPost]
        public JsonResult SaveAnalysis()
        {
            string ErrorMessage = null;

            return Json(new
            {
                Success = ResourceService.SaveRecords(out ErrorMessage, User.Identity.Name),
                ErrorMessage,
            });
        }
    }
}