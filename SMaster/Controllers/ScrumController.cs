﻿using SMaster.Entities;
using SMaster.Enums;
using SMaster.Helpers;
using SMaster.Models;
using SMaster.Models.Sprints;
using SMaster.Services;
using SMaster.Services.Sprints;
using SMaster.ViewModels.Sprint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers
{
    public class ScrumController : Controller
    {
        private SprintPlanService _sprintService;
        private ProductService _productService;
        public ScrumController()
        {
            _sprintService = new SprintPlanService();
            _productService = new ProductService();
        }

        public ActionResult AllActivities(TaskListModel model, int Success = 0)
        {
            var LoggedUserName = User.Identity.Name;

            ViewBag.Success = Success;

            model.LoadData(_sprintService, _productService, ReportingType.DailyReporting);

            return View(model);
        }

        public ActionResult PendingActivities(TaskListModel model, int Success = 0)
        {
            model.IsReminder = true;

            var LoggedUserName = User.Identity.Name;

            ViewBag.Success = Success;
            model.BacessValidity();
            model.LoadData(_sprintService, _productService, ReportingType.AllPending);

            return View(model);
        }

        [HttpPost]
        public JsonResult SaveMyScrum(SprintTaskHistory MyCurrentTasks)
        {
            string ErrorMessage = null;

            return Json(new
            {
                Success = _sprintService.SaveDailyTask(MyCurrentTasks, out ErrorMessage, User.Identity.Name),
                ErrorMessage,
                DateToComplete=MyCurrentTasks.DateToComplete?.ToString("dd MMM yyyy")??""
            });
        }
    }
}