﻿using SMaster.Models;
using SMaster.Models.Setups;
using SMaster.Services.Sprints;
using SMaster.ViewModels.Sprint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers.Sprints
{
    public class MeetingController : Controller
    {
        private Repository database;
        public MeetingController()
        {
            database = new Repository();
        }
        // GET: Meeting
        public ActionResult Index(MeetingListModel model)
        {
            model.LoadData();
            return View(model);
        }

        public ActionResult Details(int? Id)
        {
            return View(Id == null ? new Meeting() : MeetingService.GetMeetings(database).Where(r => r.Id == Id).Single());
        }


        [HttpPost]
        public ActionResult Details(Meeting model)
        {
            if (ModelState.IsValid)
            {
                List<string> Errors = new List<string>();
                Errors = MeetingService.Save(model);

                if(Errors.Count() == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach(var item in Errors)
                    {
                        model.ErrorMessage = item + "<br />";
                    }
                    return View(model);
                }
            }
            else
            {
                model.ErrorMessage = "Please fill all required fields!";
            }

            return View(model);
        }

        public ActionResult Delete(int Id)
        {
            string ErrorMessage = null;
            MeetingService.DeleteRecord(Id, out ErrorMessage);

            return RedirectToAction("Index");
        }
    }
}