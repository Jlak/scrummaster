﻿using SMaster.Enums;
using SMaster.Helpers;
using SMaster.Models.Setups;
using SMaster.Models.Sprints;
using SMaster.Services.Sprints;
using SMaster.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Controllers.Sprints
{
    public class SprintPlanController : Controller
    {
        private SprintPlanService _sprintService;
        public SprintPlanController()
        {
            _sprintService = new SprintPlanService();
        }
        // GET: Sprint
        #region Manage Sprints

        public ActionResult Index(SprintsListModel model)
        {
            model.FilteredSprint(_sprintService);

            return View(model);
        }

        public ActionResult EditDetails(int? Id, ReportingType? selectedType)
        {
            ViewBag.Type = selectedType;

            return View(Id == null ? new SprintPlan() : _sprintService.GetPlannedsprints().Where(r => r.Id == Id).Single());
        }


        [HttpPost]
        public ActionResult EditDetails(SprintPlan model, ReportingType? selectedType)
        {
            if(model.selectedType == ReportingType.ExtraActivity)
            {
                if (_sprintService.SaveSprintPlan(model))
                {
                    return RedirectToAction("Index");
                }
            }
            if (ModelState.IsValid)
            {
                if (_sprintService.SaveSprintPlan(model))
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                model.ErrorMessage = "Please fill all required fields!";
            }

            return View(model);
        }

        public ActionResult ViewDetails(int Id)
        {
            return View(_sprintService.GetPlannedsprints().Where(r => r.Id == Id).Single());
        }

        public ActionResult Delete(int Id)
        {
            string ErrorMessage = null;
            _sprintService.DeleteSprintPlan(Id, out ErrorMessage);

            return RedirectToAction("Index");
        }

        #endregion

        #region Partials

        [HttpPost]
        public PartialViewResult PartialTasks(ReportingType? selectedType)
        {
            if(selectedType == ReportingType.ExtraActivity)
            {
                return PartialView("Partials/_PartialExtraActivity", new SprintTask());
            }
            else
            {
                return PartialView("Partials/_PartialTasks", new SprintTask());
            }
        }

        public JsonResult GetClients(int ProjectId = 0)
        {
            return Json(LookupsHelper.GetProjectClients(ProjectId));
        }
        #endregion
    }
}