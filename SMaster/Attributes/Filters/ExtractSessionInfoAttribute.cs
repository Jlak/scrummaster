﻿using FSDU.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Attributes.Filters
{
    public class ExtractSessionInfoAttribute:AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                SessionUserInfo.ExtractMVCUserInfo();
            }
        }
    }
}