﻿using SMaster.Helpers;
using SMaster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FSDU.Attributes.Filters
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class InitializeDBAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (var database = Repository.Create())
            {
                SMasterUserManager.Initialize(database);

                database.SaveChanges();
            }
        }
    }
}
