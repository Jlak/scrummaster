﻿using SMaster.Models.Security;
using System.ComponentModel.DataAnnotations;

namespace SMaster.Attributes
{
    public class UserPasswordValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string password = (string)value;

            var userObject = ((ApplicationUser)validationContext.ObjectInstance);

            if (userObject.IsNewUser && string.IsNullOrEmpty(password))
            {
                return new ValidationResult("The Password is required!");
            }

            return ValidationResult.Success;
        }
    }
}
