﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Enums
{
    public enum ProgramingLanguages
    {
        Java = 0,
        Net = 1,
        Php = 2
    }

    public enum ActivityType
    {
        Developement = 0,
        QA = 1,
        Management = 2
    }
}
