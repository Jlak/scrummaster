﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Enums
{
    public enum ExportMode
    {
        EXCEL = 1,
        PDF = 2
    }
}
