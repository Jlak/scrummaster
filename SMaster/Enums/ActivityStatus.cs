﻿namespace SMaster.Enums
{
    public enum ActivityStatus
    {
        Pending = 0,
        Ongoing = 1,
        Done = 2,
        Cancelled = 3
    }
    public enum ReleaseStatus
    {
        Pending = 0,
        Done =1,
    }
    public enum SprintState
    {
        Ongoing = 0,
        Completed = 1,
    }

    public enum ReportingType
    {
        DailyReporting = 0,
        AllPending = 1,
        ExtraActivity =2
    }

    public enum ExtraActivityType
    {
        Leave = 4,
        Public_Holiday = 5,
    }
}
