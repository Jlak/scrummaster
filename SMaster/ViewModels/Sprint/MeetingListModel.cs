﻿using PagedList;
using SMaster.Models;
using SMaster.Models.Setups;
using SMaster.Services;
using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SMaster.ViewModels.Sprint
{
    public class MeetingListModel
    {
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Page { get; set; }
        public string SearchTerm { get; set; }
        public List<Meeting> Items { get; set; }
        public List<CalendarItemModel> MeetingsCalendar { get; set; }
        public IPagedList<PublicHoliday> PublicHolidays { get; set; }
        public MeetingListModel()
        {
            Items = new List<Meeting>();
            MeetingsCalendar = new List<CalendarItemModel>();
        }

        public object GetRouteValues(int Page)
        {
            return new
            {
                Page,
                Year,
                Month,
                SearchTerm
            };
        }
        public void LoadData()
        {
            using (var database = Repository.Create(false))
            {
                if (Year == null)
                {
                    Year = DateTime.Today.Year;
                }

                if (Month == null)
                {
                    Month = DateTime.Today.Month;
                }

                var query = database.Meetings.Select(r => r);

                DateTime StartDate = new DateTime(Year.Value, Month.Value, 1, 0, 0, 0);
                DateTime EndDate = new DateTime(Year.Value, Month.Value, 1, 23, 59, 59).AddMonths(1).AddDays(-1);

                query = query.Where(r =>
                            (r.StartDate >= StartDate && r.StartDate <= EndDate) ||
                            (r.EndDate >= StartDate && r.EndDate <= EndDate) ||
                            (StartDate >= r.StartDate && StartDate <= r.EndDate)); 

                foreach (var term in UtilityService.GetSearchTerms(SearchTerm))
                {
                    query = query.Where(r => r.EventName.Contains(term));
                }

                Items = query.Include(r => r.Attendees).OrderBy(r => r.StartDate).ToList();

                LoadCalendarItems(StartDate, EndDate);
            }
        }

        public void LoadHoliday(int PageSize = 15)
        {
            using (var database = Repository.Create(false))
            {
                var query = database.PublicHolidays.Select(r => r);
                PublicHolidays = query.OrderBy(r => r.EndDate).ToPagedList(Page ?? 1, PageSize);
            }
        }
        public IEnumerable<int> GetYears()
        {
            for (var i = DateTime.Today.Year + 1; i >= 2016; i--)
            {
                yield return i;
            }
        }

        protected void LoadCalendarItems(DateTime StartDate, DateTime EndDate)
        {
            for (var Date = StartDate; Date <= EndDate; Date = Date.AddDays(1))
            {
                var calendarItem = new CalendarItemModel
                {
                    Day = Date.Day
                };

                var eventsFound = Items.Where(r => (r.StartDate <= Date || r.StartDate.Value.Day <= Date.Day) && (r.EndDate >= Date || Date.Day <= r.EndDate.Value.Day))
                    .Select(r => (object)new
                    {
                        r.EventName,
                        Location = r.Venue ?? "",
                        StartDate = r.StartDate?.ToString("dd - MMM -yyyy"),
                        EndDate = r.EndDate?.ToString("dd - MMM -yyyy"),
                        AttendingNo = r.Attendees.Count()
                    }).ToList();

                if (eventsFound.Count == 0) continue;

                calendarItem.AddEvents(eventsFound);

                MeetingsCalendar.Add(calendarItem);
            }
        }
    }
    public class CalendarItemModel
    {
        public int Day { get; set; }
        public List<object> Events { get; set; }

        public bool HasEvents
        {
            get
            {
                return Events.Count > 0;
            }
        }

        public CalendarItemModel()
        {
            Events = new List<object>();
        }

        public void AddEvents(List<object> Events)
        {
            this.Events.AddRange(Events);
        }
    }
}