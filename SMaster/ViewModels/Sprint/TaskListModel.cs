﻿using SMaster.Entities;
using SMaster.Enums;
using SMaster.Helpers;
using SMaster.Models;
using SMaster.Models.Security;
using SMaster.Models.Sprints;
using SMaster.Services;
using SMaster.Services.Sprints;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SMaster.ViewModels.Sprint
{
    public class TaskListModel
    {
        public string GivenString { get; set; }
        [DisplayName("Project Name")]
        public int? ProjectId { get; set; }
        public int? SprintId { get; set; }
        [DisplayName("User Name")]
        public string UserId { get; set; }
        public string UserFullName { get; set; }
        public bool IsReminder { get; set; }
        [DisplayName("Activity Date")]
        public DateTime? Date { get; set; }
        public List<SMaster.Models.Sprints.SprintTaskHistory> AllTasks { get; set; }
        public List<Project> Projects { get; set; }
        public List<ApplicationUser> Assignee { get; set; }
        public TaskListModel()
        {
            Projects = new List<Project>();
            Assignee = new List<ApplicationUser>();
        }

        public bool BacessValidity()
        {
            string openingBraces = "[{(";
            string closingBraces = "]})";
            Stack<int> stack =  new Stack<int>();
            if(string.IsNullOrEmpty(GivenString))
            {
                GivenString = "";
            }
            foreach (char c in GivenString)
            {
                if (openingBraces.Contains(c))
                 {
                    // push on to stack when open character received
                    stack.Push(openingBraces.IndexOf(c));
                }
                else if (closingBraces.Contains(c))
                {
                    // make sure it’s a valid closing
                    int last = (int)stack.Pop();
                    if (last != closingBraces.IndexOf(c))
                        return false;
                 }
                 else
                 {
                    // must be an opening or closing
                    return false;
                }
            }
            return true;
        }
        public void LoadData(SprintPlanService _sprintService, ProductService productService, ReportingType reportType)
        {
            var database = _sprintService.GetDatabase();

            Projects = database.Projects.OrderBy(r => r.Name).ToList();
            Assignee = database.Users.Where(r=>r.IsActive).Distinct().ToList();

            var query = from a in database.SprintTaskHistories.Where(r=>r.Assignee.SprintTask.ExtraActivity == null) select a;

            if(reportType == ReportingType.AllPending)
            {
                if(!string.IsNullOrEmpty(UserId))
                {
                    query = query.Where(r => r.Assignee.UserId == UserId);
                }
                var reportinDate = DateTime.Today.AddDays(-1);
                query = query.Where(r => r.Status < ActivityStatus.Done && r.ActivityDate < reportinDate && r.DateToComplete == null);
            }
            else
            {
                if (Date == null)
                {
                    Date = DateTime.Today;
                }

                query = from a in query
                        where a.ActivityDate == Date
                            select a;
            }

            if (ProjectId.HasValue)
            {
                query = query.Where(r => r.Assignee.SprintTask.SprintPlan.ProjectId == ProjectId);
            }

            if (!HttpContext.Current.User.IsInRole("Administrator"))
            {
                var LoggedUserName = HttpContext.Current.User.Identity.Name;

                var CurrentUserId = database.Users.Where(r => r.UserName == LoggedUserName).Select(r => r.Id).Single();

                if (HttpContext.Current.User.IsInRole(SMasterUserManager.PROJECT_MANAGER))
                {
                    query = query.Where(r => r.Assignee.SprintTask.SprintPlan.Project.ProjectManagerId == CurrentUserId || r.Assignee.UserId == CurrentUserId);
                }
                else
                {
                    query = query.Where(r => r.Assignee.UserId == CurrentUserId);
                }
            }

            AllTasks = query.Include(r=>r.Assignee).Include(r=>r.Assignee.SprintTask).Include(r=>r.Assignee.SprintTask.SprintPlan).ToList();
        }
    }
}