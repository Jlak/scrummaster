﻿using PagedList;
using SMaster.Entities;
using SMaster.Models.Sprints;
using SMaster.Services;
using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.ViewModels.Sprints
{
    public class SprintViewModel
    {
        public string FilterSprintName { get; set; }
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }
        public string Status { get; set; }
        public int? Page { get; set; }
        [DisplayName("Project Name")]
        public int? ProjectId { get; set; }
        public string SortOrder { get; set; }
        public string SearchTerm { get; set; }
        public IPagedList<SprintPlan> Sprints { get; set; }
        public object GetRouteValues(int Page)
        {
            return new
            {
                Page,
                FilterSprintName,
                StartDate,
                EndDate,
                Status,
                SortOrder,
                SearchTerm
            };
        }
        public void FilteredSprint(SprintPlanService _Sprintservice, int PageSize = 20)
        {
            if (ProjectId != null)
            {
                var Query = _Sprintservice.GetPlannedsprints();

                if (StartDate.HasValue)
                {
                    Query = Query.Where(r => r.StartDate == StartDate);
                }

                if (StartDate.HasValue)
                {
                    Query = Query.Where(r => r.EndDate == EndDate);
                }
                if (ProjectId != null)
                {
                    Query = Query.Where(r => r.ProjectId == ProjectId);
                }

                if (!string.IsNullOrEmpty(Status))
                {
                    Query = Query.Where(r => r.Status.ToString().Contains(Status));
                }

                Sprints = Query.OrderByDescending(r => new { r.StartDate }).ToPagedList(Page ?? 1, PageSize);
            }
        }
    }
}