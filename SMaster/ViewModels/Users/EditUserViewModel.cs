﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.ViewModels.Users
{
    public class EditUserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        [MaxLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [MaxLength(50)]
        [Display(Name = "Other Name")]
        public string OtherName { get; set; }
        public string Initials { get; set; }
        [EmailAddress]
        [Required(ErrorMessage = "*")]
        public string Email { get; set; }
        [Phone]
        [Display(Name = "Phone No.")]
        public string Phone { get; set; }
        [Display(Name = "Role"), Required(ErrorMessage = "*")]
        public string UserRole { get; set; }
        [Display(Name = "Daily Rate")]
        public decimal? DailyRate { get; set; }
        [Display(Name = "Is User Active")]
        public bool IsActive { get; set; }
    }
}
