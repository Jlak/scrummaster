﻿using FSDU.Helpers;
using PagedList;
using SMaster.Helpers;
using SMaster.Service.ControlPanel;

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SMaster.ViewModels.Users
{
    public class UsersListViewModel
    {
        #region Filters

        public int? Page { get; set; }
        [Display(Name = "User Name")]
        public string UserId { get; set; }
        public bool IsActive { get; set; }
        public bool CanCreateSprint { get; set; }
        public string SearchTerm { get; set; }

        public IPagedList<UserListItemModel> Users { get; set; }

        #endregion

        public object GetRouteValues(int Page)
        {
            return new
            {
                Page,
                UserId,
                SearchTerm,
                IsActive
            };
        }

        /// <summary>
        /// Loads Data for System Admins
        /// </summary>
        /// <param name="_controlpanelService"></param>
        public void LoadData(ControlPanelService _controlpanelService)
        {
            var query = _controlpanelService.GetUsers();

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query = query.Where(r => r.UserName.Contains(SearchTerm) || r.FirstName.Contains(SearchTerm) || r.LastName.Contains(SearchTerm)
                || r.OtherName.Contains(SearchTerm));
            }

            if (UserId != null)
            {
                query = query.Where(r => r.Id == UserId);
            }

            if (IsActive)
            {
                query = query.Where(r => r.IsActive);
            }

            //Isoldate admin roles
            var allRoles = _controlpanelService.GetUserRoles().ToList();
            //Extract Ids
            var allRoleIds = allRoles.Select(r => r.Id).ToList();
            //Extract root role Id
            var rootAdministratorRoleId = allRoles.Where(r => r.Name == SMasterUserManager.SuperRole).Select(r => r.Id).Single();

            var rootScrumRoleId = allRoles.Where(r => r.Name == SMasterUserManager.SCRUM_MASTER).Select(r => r.Id).Single();

            var rootDevRoleId = allRoles.Where(r => r.Name == SMasterUserManager.DEVELOPER).Select(r => r.Id).Single();

            var rootQARoleId = allRoles.Where(r => r.Name == SMasterUserManager.QUALITY_ASSURANCE).Select(r => r.Id).Single();

            var rootPMRoleId = allRoles.Where(r => r.Name == SMasterUserManager.PROJECT_MANAGER).Select(r => r.Id).Single();

            Users = (from a in query
                     from b in a.Roles
                     where allRoleIds.Contains(b.RoleId)
                     select new UserListItemModel
                     {
                         FirstName = a.FirstName,
                         LastName = a.LastName,
                         OtherName = a.OtherName,
                         Role = b.RoleId == rootAdministratorRoleId ? SMasterUserManager.SuperRole : (b.RoleId == rootScrumRoleId ? SMasterUserManager.SCRUM_MASTER :(b.RoleId == rootDevRoleId ? SMasterUserManager.DEVELOPER : (b.RoleId == rootQARoleId ? SMasterUserManager.QUALITY_ASSURANCE : (b.RoleId == rootPMRoleId ? SMasterUserManager.PROJECT_MANAGER : SMasterUserManager.REGULAR_USER)))),
                         UserId = a.Id,
                         UserName = a.UserName,
                         Email = a.Email,
                         ProgramingLanguage = a.ProgramingLanguage,
                         Initials = a.Initials,
                         IsActive = a.IsActive,
                     }
                     ).OrderBy(r => r.UserName).ToPagedList(Page ?? 1, 10);
        }
        
        /// <summary>
        /// Loads Data for Organisation Users
        /// </summary>
        /// <param name="_controlpanelService"></param>
        public void LoadRegularUserData(ControlPanelService _controlpanelService)
        {
            var currentUserId = SessionUserInfo.GetCurrentUserInfo().AppUser.Id;

            var query = _controlpanelService.GetUsers().Where(r => r.Id == currentUserId);

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query = query.Where(r => r.UserName.Contains(SearchTerm) || r.FirstName.Contains(SearchTerm) || r.LastName.Contains(SearchTerm)
                || r.OtherName.Contains(SearchTerm));
            }

            if (IsActive)
            {
                query = query.Where(r => r.IsActive);
            }
            //Isoldate admin roles
            var regularUserRoleId = _controlpanelService.GetUserRoles()
                .Where(r => r.Name == SMasterUserManager.REGULAR_USER).Select(r => r.Id).Single();

            Users = (from a in query
                     from b in a.Roles
                     where b.RoleId == regularUserRoleId
                     select new UserListItemModel
                     {
                         FirstName = a.FirstName,
                         LastName = a.LastName,
                         OtherName = a.OtherName,
                         UserId = a.Id,
                         UserName = a.UserName,
                         Email = a.Email,
                         ProgramingLanguage = a.ProgramingLanguage,
                         Initials = a.Initials,
                         IsActive = a.IsActive,
                     }
                     ).OrderBy(r => r.UserName).ToPagedList(Page ?? 1, 15);
        }
    }

    public class UserListItemModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public string Initials { get; set; }
        public string ProgramingLanguage { get; set; }
        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2}", FirstName, LastName, OtherName);
            }
        }
    }
}
