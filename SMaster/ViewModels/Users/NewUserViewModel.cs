﻿using SMaster.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace SMaster.ViewModels.Users
{
    public class NewUserViewModel : IErrorMessage
    {
        [MaxLength(30), Required(ErrorMessage = "*"), RegularExpression("[a-z,A-Z]{1}[a-z,A-Z,0-9,_]{1,}", ErrorMessage = "Invalid characters!")]
        [Display(Name ="User Name")]
        public string UserName { get; set; }
        [MaxLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [MaxLength(50)]
        [Display(Name = "Other Name")]
        public string OtherName { get; set; }
        [EmailAddress]
        [Required(ErrorMessage = "*")]
        public string Email { get; set; }
        [Phone]
        [Display(Name = "Phone No.")]
        public string Phone { get; set; }
        public string Initials { get; set; }
        [Display(Name = "Programing Language")]
        public string ProgramingLanguage { get; set; }
        public string ErrorMessage { get; set; }
        [Display(Name ="Role"),Required(ErrorMessage="*")]
        public string UserRole { get; set; }
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }
        [Display(Name = "Is User Active")]
        public bool IsActive { get; set; }
        [Display(Name = "Daily Rate")]
        public decimal DailyRate { get; set; }
        public int CurrencyId { get; set; }
    }
}
