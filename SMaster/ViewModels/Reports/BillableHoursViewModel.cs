﻿using SMaster.Entities;
using SMaster.Enums;
using SMaster.Helpers;
using SMaster.Models.Security;
using SMaster.Models.Sprints;
using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMaster.ViewModels.Reports
{
    public class BillableHoursViewModel
    {
        public string SearchTerm { get; set; }
        public int? ProjectId { get; set; }
        public ExportMode? Export { get; set; }
        [DisplayName("Staff")]
        public string UserId { get; set; }
        public DateTime? Date { get; set; }
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; } 
        public bool IncludeInactiveUsers { get; set; }
        public List<BillableHoursField> BillableHours { get; set; }
        public List<Project> Projects { get; set; }
        public List<ApplicationUser> Users { get; set; }
        public List<SprintTaskHistory> Histories { get; set; }

        public object GetRouteValues(int Page, ExportMode? Export = null)
        {
            return new
            {
                Page,
                StartDate = StartDate?.ToString("yyyy-MM-dd"),
                EndDate = EndDate?.ToString("yyyy-MM-dd"),
                UserId,
                Export
            };
        }

        public void LoadChargeableHours(SprintPlanService _sprintService)
        {
            var AllTasks = _sprintService.GetPlannedTaskHistories();
            if (StartDate == null)
            {
                StartDate = DateTime.Today.AddDays(-6);
            }

            if (EndDate == null)
            {
                EndDate = DateTime.Today;
            }

            if (StartDate.HasValue)
            {
                AllTasks = AllTasks.Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate);
            }

            var Qry1 = from a in AllTasks
                       select new
                       {
                           ProjectName = a.Assignee.SprintTask.SprintPlan.Project.ShortName,
                           ActualHours = a.ActualHours > 0 ? a.ActualHours.Value : 0,
                           ProjectDate = a.ActivityDate.Value,
                           Activity = a.Assignee.SprintTask.Description,
                           UserId = a.Assignee.User.Id,
                           UserName = a.Assignee.User.FirstName + " " + a.Assignee.User.LastName + " " + a.Assignee.User.OtherName,
                       };

            if (!string.IsNullOrEmpty(UserId))
            {
                Qry1 = Qry1.Where(r => r.UserId == UserId);
            } 

            foreach (var term in ViewHelper.GetSearchTerms(SearchTerm))
            {
                Qry1 = Qry1.Where(r => r.UserName.Contains(term) || r.ProjectName.Contains(term) || r.Activity.Contains(term) || r.ActualHours.ToString().Contains(term));
            }

            var Qry2 = from a in Qry1
                       group a by new { a.ProjectDate, a.UserName, a.ProjectName, a.Activity } into Results

                       select new
                       {
                           ProjectDate = Results.Key.ProjectDate,
                           UserName = Results.Key.UserName,
                           ProjectName = Results.Key.ProjectName,
                           Activity = Results.Key.Activity,
                           Hours = Results.Sum(r => r.ActualHours),
                       };


            var Data = from a in Qry2
                       group a by new { a.ProjectDate, a.UserName } into Results
                       select new BillableHoursField
                       {
                           ProjectDate = Results.Key.ProjectDate,
                           UserName = Results.Key.UserName,
                           Hours = Results.Sum(r => r.Hours),
                          
                           ActivityField = (from a in Qry2.Where(r=>r.UserName==Results.Key.UserName && r.ProjectDate== Results.Key.ProjectDate)
                                            select new ActivityFields
                                            {
                                                Activity = a.Activity,
                                                Hours = a.Hours,
                                                ProjectName = a.ProjectName
                                            }).ToList()
                       };
            BillableHours = Data.OrderByDescending(r => r.UserName).ToList();

        }

        public void LoadExpenditure(SprintPlanService _sprintService)
        {
            if (StartDate == null)
            {
                StartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            }

            if (EndDate == null)
            {
                EndDate = StartDate.Value.AddDays(30);
            }

            Users = _sprintService.GetUsers().Where(r => r.IsActive).ToList();

            if (!string.IsNullOrEmpty(UserId))
                Users = Users.Where(r => r.Id == UserId).ToList();

            if (!string.IsNullOrEmpty(SearchTerm))
                Users = Users.Where(r => r.UserName.Contains(SearchTerm) || r.FirstName.Contains(SearchTerm) || r.LastName.Contains(SearchTerm)).ToList();

            if (IncludeInactiveUsers)
            {
                Users = _sprintService.GetUsers().ToList();
            }

            Projects = _sprintService.GetPlannedTaskHistories()
                    .Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate)
                    .Select(r=>r.Assignee.SprintTask.SprintPlan.Project).Distinct().ToList();
            Histories = _sprintService.GetPlannedTaskHistories().Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate).ToList();
        }
    }


    public class BillableHoursField
    {
        public DateTime ProjectDate { get; set; }
        public decimal Hours { get; set; }
        public string UserName { get; set; }
        public string Activity { get; set; }
        public List<ActivityFields> ActivityField { get; set; }
    }
    public class ActivityFields
    {
        public string Activity { get; set; }
        public decimal Hours { get; set; }
        public string ProjectName { get; set; }
    }
    public class UserProject
    {
        public string UserName { get; set; }
        public List<ProjectTime> ProjectTimes { get; set; }
        public UserProject()
        {
            ProjectTimes = new List<ProjectTime>();
        }
    }
    public class ProjectTime
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public decimal? Hours { get; set; }
    }
}