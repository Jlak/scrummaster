﻿using SMaster.Enums;
using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SMaster.ViewModels.Dashboard
{
    public class ProjectAnalysisListModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UserId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public AnalysisModel Details { get; set; }
        public object GetRouteValues(int Page, ExportMode? Export = null)
        {
            return new
            {
                Page,
                Year = Year,
                Month = Month,
                UserId,
                Export
            };
        }
        public void LoadProjectAnalysis(SprintPlanService _sprintService)
        {
            var Query = _sprintService.GetPlannedTaskHistories().Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate && r.Assignee.UserId == UserId);

            Details = (from a in Query
                       select new AnalysisModel
                       {
                           Recipient = "Finance and Administration Department",
                           RecipientCompanyAddress = "Data Care (U) LTD,P.o Box 1170 Kampala,Plot 2A Kyambogo Drive,Kampala,Uganda",
                           EmployeeName = a.Assignee.User.FirstName + " " + a.Assignee.User.LastName + " " + a.Assignee.User.OtherName,
                           EmpId = a.Assignee.User.Id,
                           EmpCompanyAddress = "Data Care (U) LTD,P.o Box 1170 Kampala,Plot 2a Kyambogo Drive,Kampala,Uganda",
                           AttentionTo = "Patrick Kagenda",
                           Date = DateTime.Now,
                           Items = Query.GroupBy(r=>r.Assignee.SprintTask.SprintPlan.ProjectId).Select(r => new AnalysisModelItem
                           {
                               ProjectId = r.Select(m=>m.Assignee.SprintTask.SprintPlan.ProjectId).FirstOrDefault(),
                               ProjectName = r.Select(m => m.Assignee.SprintTask.SprintPlan.Project.ShortName).FirstOrDefault(),
                               Hours = r.Sum(l=>l.ActualHours),
                               RatePerHour = a.Assignee.User.DailyRate,
                           }).ToList()
                       }).FirstOrDefault();
        }

        public class AnalysisModel
        {
            public string Recipient { get; set; }
            public string RecipientCompanyAddress { get; set; }
            public string EmployeeName { get; set; }
            public string EmpId { get; set; }
            public string EmpCompanyAddress { get; set; }
            public string AttentionTo { get; set; }
            public DateTime Date { get; set; }
            public List<AnalysisModelItem> Items { get; set; }
            public AnalysisModel()
            {
                Items = new List<AnalysisModelItem>();
            }
        }

        public class AnalysisModelItem
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public decimal? Hours { get; set; }
            public decimal? RatePerHour { get; set; }
        }
    }
}