﻿using SMaster.Models.Reports;
using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMaster.ViewModels.Dashboard
{
    public class ProjectResourceListModel
    {
        public int? ProjectId { get; set; }
        public bool Export { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? startOfWeek { get; set; }
        public DayOfWeek? DayOfWeek { get; set; }
        public List<ProjectAnalysis> Details { get; set; }
        public object GetRouteValues(int Page, bool Export = false)
        {
            return new
            {
                Page,
                ProjectId,
                Export
            };
        }

        public void LoadProjectAnalysis(SprintPlanService _sprintService)
        {
            var Query = _sprintService.GetProjectAnalysis();

            Details = Query.OrderBy(r => r.ProjectName).ToList();
        }
    }
}
