﻿using SMaster.Entities;
using SMaster.Enums;
using SMaster.Helpers;
using SMaster.Models.Sprints;
using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SMaster.ViewModels.Dashboard
{
    public class DashboardListModel
    {
        public int? ProjectId { get; set; }
        public bool Export { get; set; }
        [Display(Name = "User Name")]
        public string UserId { get; set; }
        public DateTime? Date { get; set; }
        [Display(Name= "Start Date")]
        public DateTime ? StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        public string UserName { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public DateTime? startOfWeek { get; set; }
        public DayOfWeek? DayOfWeek { get; set; }
        public List<ProjectCosting> ProjectCost { get; set; }
        public List<string> UnallocatedUsers { get; set; }
        public List<Project> Projects { get; set; }
        public List<SprintTaskHistory> AllTasks { get; set; }
        public List<UsersTimes> Timing { get; set; }
        public List<ProjectWithHours> ProjectWithHour { get; set; }
        public List<UsersLevelOfEffort> Effort { get; set; }
        public List<UsersLevelOfEffort> TopProject { get; set; }
        public List<FrequencyReport> FrequencyReports { get; set; }
        public DateTime? LastReportDate { get; set; }
        public DashboardListModel()
        {
            UnallocatedUsers = new List<string>();
            Projects = new List<Project>();
            AllTasks = new List<SprintTaskHistory>();
            Timing = new List<UsersTimes>();
            ProjectCost = new List<ProjectCosting>();
            FrequencyReports = new List<FrequencyReport>();
        }
        public object GetRouteValues(int Page, DateTime? StartDate, DateTime? EndDate, string UserId, bool Export = false)
        {
            return new
            {
                Page,
                StartDate = StartDate?.ToString("yyyy-MM-dd"),
                EndDate = EndDate?.ToString("yyyy-MM-dd"),
                ProjectId,
                UserId, 
                Export,
                Month,
                Year
            };
        }

        public void LoadData(SprintPlanService _sprintService)
        {
            var CurrentUserName = HttpContext.Current.User.Identity.Name;
            var UserDetails = _sprintService.GetUsers().Where(r=>r.UserName == CurrentUserName).FirstOrDefault();
            if(UserDetails != null)
            {
                var Assignees = _sprintService.GetPlannedTaskHistories().Where(r => r.Assignee.UserId == UserDetails.Id);
                if(Assignees != null)
                {
                    LastReportDate = Assignees.Where(r => r.Assignee.UserId == UserDetails.Id).OrderByDescending(r => r.ActivityDate).Max(l => l.ActivityDate);
                }
            }
        }

        public void LoadFrequency(SprintPlanService _sprintService)
        {
            Year = (Year == 0 ? DateTime.Today.Year : Year);
            Month = (Month == 0 ? DateTime.Today.Month : Month);
            var AllUsers = _sprintService.GetUsers();
            if (!string.IsNullOrEmpty(UserId))
            {
                AllUsers = AllUsers.Where(r => r.Id == UserId);
            }
            var Tasks = _sprintService.GetPlannedTaskHistories().Include(r=>r.Assignee).Include(r => r.Assignee.SprintTask)
                .Include(r => r.Assignee.SprintTask.SprintPlan).Include(r => r.Assignee.SprintTask.SprintPlan.Project).ToList();           
            foreach (var user in AllUsers)
            {
                var ModelList = new List<FrequnecyReportItem>();
                foreach (var dayInMonth in _GetDays(Year, Month))
                {
                    ModelList.Add(new FrequnecyReportItem
                    {
                        Day = dayInMonth,
                        HoursWorked = _GetHours(_sprintService, user.Id, dayInMonth, Tasks),
                        TaskName = _GetTaskName(_sprintService, user.Id, dayInMonth, Tasks)
                    });
                }
                FrequencyReports.Add(new FrequencyReport
                {
                    UserId = user.Id,
                    UserName = user?.FirstName + " " + user?.LastName + " " + user?.OtherName,
                    Items = ModelList,
                });
            }
        }

        private List<DateTime> _GetDays(int Year, int Month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(Year, Month))
                .Select(day => new DateTime(Year, Month, day))
                .ToList();
        }

        private string _GetTaskName(SprintPlanService _sprintService, string UserId, DateTime dayInMonth, List<SprintTaskHistory> Tasks)
        {
            string TaskName = "";
            var data = Tasks.Where(r => r.Assignee.UserId == UserId && r.ActivityDate == dayInMonth).ToList();
            if(data != null && (data.Sum(r => r.ActualHours) > 0))
            {
                foreach(var item in data)
                {
                    TaskName += "<b>"+ item.Assignee.SprintTask.SprintPlan.Project.ShortName +": </b> "+ item.Assignee.SprintTask.Description + "<br />";
                }
            }
            return TaskName;
        }
        private decimal _GetHours(SprintPlanService _sprintService, string UserId, DateTime dayInMonth, List<SprintTaskHistory> Tasks)
        {
            var data = Tasks.Where(r => r.Assignee.UserId == UserId && r.ActivityDate == dayInMonth).ToList();
            var Results = data.Sum(r => r.ActualHours) ?? 0;
            return Results;
        }
        public void LoadUnallocatedUsers(SprintPlanService _sprintService)
        {
            var AllTasks = _sprintService.GetPlannedTaskHistories();
            var AllUsers = _sprintService.GetUsers();

            if (!Date.HasValue)
            {
                Date = DateTime.Today;
            }

            if (!string.IsNullOrEmpty(UserId))
            {
                AllUsers = AllUsers.Where(r => r.Id == UserId);
            }

            AllTasks = AllTasks.Where(r => r.ActivityDate == Date);
            
            var QueryLevel1 = (from a in AllTasks
                               where a.ActivityDate == Date
                               select a.Assignee.UserId).Distinct();

            var QueryLevel2 = (from a in AllUsers
                               where !QueryLevel1.Contains(a.Id)
                               orderby a.FirstName
                               select a.FirstName +" "+ a.LastName + " "+ a.OtherName).ToList();

            UnallocatedUsers = QueryLevel2;

        }
        public void LoadWeeklyData(SprintPlanService _sprintService)
        {
            Projects = (from a in _sprintService.GetPlannedTaskHistories().Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate)
                        select a.Assignee.SprintTask.SprintPlan.Project).Distinct().OrderBy(r => r.Name).ToList();
            
            var PendingTasks = from a in _sprintService.GetPlannedTaskHistories().Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate)
                               select a;

            if (ProjectId.HasValue)
            {
                PendingTasks = PendingTasks.Where(r => r.Assignee.SprintTask.SprintPlan.ProjectId == ProjectId);
            }

            if(!string.IsNullOrEmpty(UserId))
            {
                PendingTasks = PendingTasks.Where(r => r.Assignee.UserId == UserId);
            }

            AllTasks = PendingTasks.Include(r => r.Assignee.SprintTask.SprintPlan).OrderBy(r=>r.ActivityDate).ToList();
        }
        public void LoadProjectCosting(SprintPlanService _sprintService)
        {
            var AllTasks = _sprintService.GetPlannedTaskHistories();
            if(StartDate == null)
            {
                StartDate = DateTime.Today.AddDays(-6);
            }

            if(EndDate == null)
            {
                EndDate = DateTime.Today;
            }

            if (StartDate.HasValue)
            {
                AllTasks = AllTasks.Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate);
            }

            var QueryLevel1 = from a in AllTasks
                              select new
                              {
                                  ProjectId = a.Id,
                                  ProjectName = a.Assignee.SprintTask.SprintPlan.Project.ShortName,
                                  TaskStatus = a.Status,
                                  a.Assignee.SprintTask.PlannedHours,
                                  a.ActualHours,
                                  ActivityType = a.Assignee.SprintTask.Activitytype,
                              };


            var QueryLevel2 = from a in QueryLevel1
                              select new
                              {
                                  a.ProjectId,a.ProjectName,
                                  TotalTask = a.TaskStatus,
                                  a.PlannedHours, a.ActualHours, a.ActivityType,
                              };

            var QueryLevel3 = (from a in QueryLevel2
                               group a by new { a.ProjectName } into tblResults
                               select new ProjectCosting
                               {
                                   ProjectName = tblResults.Key.ProjectName,
                                   PlannedHoursManagement = tblResults.Where(r=>r.ActivityType == ActivityType.Management).Sum(r => r.PlannedHours),
                                   ActualHoursManagement = tblResults.Where(r=>r.ActivityType == ActivityType.Management).Sum(r => r.ActualHours),
                                   PlannedHoursDevelopment = tblResults.Where(r => r.ActivityType == ActivityType.Developement || r.ActivityType == ActivityType.QA).Sum(r => r.PlannedHours),
                                   ActualHoursDevelopment = tblResults.Where(r => r.ActivityType == ActivityType.Developement || r.ActivityType == ActivityType.QA).Sum(r => r.ActualHours),

                               }).ToList();

            ProjectCost = QueryLevel3.OrderByDescending(r => r.ProjectId).ToList();

        }
        public void LoadProjectPerStaff(SprintPlanService _sprintService)
        {
            var AllTasks = _sprintService.GetPlannedTaskHistories();

            if (DayOfWeek.HasValue)
            {
                AllTasks = AllTasks.Where(r => r.ActivityDate.Value.DayOfWeek == DayOfWeek);
            }


            var QueryLevel1 = from a in AllTasks
                              select new 
                              {
                                  ProjectName = a.Assignee.SprintTask.SprintPlan.Project.ShortName,
                                  PlannedHours = a.Assignee.SprintTask.PlannedHours,
                                  ActualHours = a.ActualHours,
                                  UserName = a.Assignee.User.FirstName +" "+ a.Assignee.User.LastName +" "+ a.Assignee.User.OtherName,
                              };

            var QueryLevel2 = from a in QueryLevel1
                              group a by new {a.ProjectName, a.UserName} into Results
                              select new ProjectWithHours
                              {
                                  ProjectName = Results.Key.ProjectName,
                                  PlannedHours = Results.Sum(r=>r.PlannedHours),
                                  ActualHours = Results.Sum(r => r.ActualHours),
                                  UserName = Results.Key.UserName,
                              };

            ProjectWithHour = QueryLevel2.OrderByDescending(r => r.ProjectName).ToList();

        }
        public void LoadUsersLevelOfEffort(SprintPlanService _sprintService)
        {
            if (StartDate == null && EndDate == null)
            {
                StartDate = DateTime.Today.AddDays(-6);
                EndDate = DateTime.Today;
            }

            var database = _sprintService.GetDatabase();

            var TaskHistories = database.SprintTaskHistories.Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate && r.Assignee.User.IsActive);

            var WorkingDays = GenericLookupHelper.GetNumberOfWorkingDays(StartDate.Value, EndDate.Value);

            var query = from a in TaskHistories
                        select new
                        {
                            a.Assignee.UserId,
                            UserName = a.Assignee.User.FirstName + " " + a.Assignee.User.LastName + " " + a.Assignee.User.OtherName,
                            a.ActualHours,
                            ActivityType = a.Assignee.SprintTask.ExtraActivity,
                            a.Assignee.SprintTask.PlannedHours,
                            DayWorked = a.ActivityDate
                        };

            var QueryLevel2 = from a in query
                              group a by new { a.UserName, a.UserId } into ResultSet
                              select new
                              {
                                  ResultSet.Key.UserId,
                                  StaffName = ResultSet.Key.UserName,
                                  ActualHours = ResultSet.Sum(r => r.ActualHours),
                                  LeaveHours = ResultSet.Where(r => r.ActivityType == ExtraActivityType.Leave).Sum(r => r.ActualHours),
                                  PublicDays = ResultSet.Where(r => r.ActivityType == ExtraActivityType.Public_Holiday).Sum(r => r.ActualHours),
                                  PlannedHours = ResultSet.Sum(r => r.PlannedHours),
                                  DaysWorked = (ResultSet.Select(r => r.DayWorked).Distinct().Count()),
                              };

            var result = from a in QueryLevel2
                         select new UsersLevelOfEffort
                         {
                             UserId = a.UserId,
                             UserName = a.StaffName,
                             ActualHours = a.ActualHours,
                             LeaveHours = (a.LeaveHours),
                             PublicDays = (a.PublicDays),
                             PlannedHours = a.PlannedHours,
                             ExpectedHours = WorkingDays * 8,
                             LeaveTime = a.ActualHours,
                             PercentageWorked = ((a.ActualHours == null ? 0 : a.ActualHours / (WorkingDays * 8)) * 100),
                         };

            Effort = result.OrderBy(r => r.UserName).ToList();
        }
        public void LoadTopProject(SprintPlanService _sprintService)
        {
            if (StartDate != null && EndDate != null)
            {
                var database = _sprintService.GetDatabase();

                var TaskHistories = database.SprintTaskHistories.Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate);

                var query = from a in TaskHistories
                            select new
                            {
                                a.Assignee.SprintTask.SprintPlan.ProjectId,
                                ProjectName = a.Assignee.SprintTask.SprintPlan.Project.ShortName,
                                ActualHours = a.ActualHours,
                            };

                var QueryLevel2 = from a in query
                                  group a by new { a.ProjectId, a.ProjectName } into ResultSet
                                  select new
                                  {
                                      ResultSet.Key.ProjectId,
                                      ResultSet.Key.ProjectName,
                                      ActualHours = ResultSet.Sum(r => r.ActualHours)
                                  };

                var result = from a in QueryLevel2
                             select new UsersLevelOfEffort
                             {
                                 ProjectId = a.ProjectId,
                                 ProjectName = a.ProjectName,
                                 ActualHours = a.ActualHours
                             };

                TopProject = result.OrderByDescending(r => r.ActualHours).Take(5).ToList();
            }
        }
        public class ProjectCosting
        {
            public string ProjectName { get; set; }
            public int ProjectId { get; set; }
            public decimal? ActualHoursManagement { get; set; }
            public decimal? PlannedHoursManagement { get; set; }
            public decimal? ActualHoursDevelopment { get; set; }
            public decimal? PlannedHoursDevelopment { get; set; }
        }
        public class UsersTimes
        {
            public int UserId { get; set; }
            public string UserFullName { get; set; }
            public decimal? Hours { get; set; }
            public decimal? PlannedHours { get; set; }
        }
        public class ProjectWithHours
        {
            public string ProjectName { get; set; }
            public string UserName { get; set; }
            public decimal? ActualHours { get; set; }
            public decimal? PlannedHours { get; set; }
        }
        public class UsersLevelOfEffort
        {
            public string UserId { get; set; }
            public string UserName { get; set; }
            public decimal? ActualHours { get; set; }
            public decimal? LeaveHours { get; set; }
            public decimal? PublicDays { get; set; }
            public decimal? PlannedHours { get; set; }
            public decimal? ExpectedHours { get; set; }
            public decimal? PercentageWorked { get; set; }
            public decimal? LeaveTime { get; set; }
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
        }
        public class FrequencyReport
        {
            public string  UserId { get; set; }
            public string UserName { get; set; }
            public List<FrequnecyReportItem> Items { get; set; }
            public FrequencyReport()
            {
                Items = new List<FrequnecyReportItem>();
            }
        }

        public class FrequnecyReportItem
        {
            public DateTime Day { get; set; }
            public decimal HoursWorked { get; set; }
            public string TaskName { get; set; }
        }
    }
}