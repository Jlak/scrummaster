﻿using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMaster.ViewModels.Dashboard
{
    public class ProjectCostListModel
    {
        public int? ProjectId { get; set; }
        public bool Export { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? startOfWeek { get; set; }
        public DayOfWeek? DayOfWeek { get; set; }
        public List<CostDetails> Details { get; set; }
        public object GetRouteValues(int Page, bool Export = false)
        {
            return new
            {
                Page,
                ProjectId,
                Export
            };
        }

        public void LoadExpenditure (SprintPlanService _sprintService)
        {
            var AllTasks = _sprintService.GetPlannedTaskHistories();

            var projectDetails = _sprintService.GetProjects();

            if (Date == null)
            {
                Date = DateTime.Today;
            }

            AllTasks = AllTasks.Where(r => r.ActivityDate == Date);

            var QueryLevel1 = (from a in AllTasks
                               group a by new { a.Assignee.SprintTask.SprintPlan.ProjectId} into Result
                               select new 
                               {
                                   Project = Result.FirstOrDefault().Assignee.SprintTask.SprintPlan.Project.Name,
                                   Acronyme = Result.FirstOrDefault().Assignee.SprintTask.SprintPlan.Project.ShortName,
                                   ContractValue = Result.FirstOrDefault().Assignee.SprintTask.SprintPlan.Project.Amount,
                                   Rate = Result.Select(r=>r.Assignee.User.DailyRate).FirstOrDefault() ?? 0,
                                   HoursWorked = Result.Sum(r=>r.ActualHours) ?? 0,
                               }).ToList();

            var QueryLevel2 = from a in QueryLevel1
                              select new CostDetails
                              {
                                  Project = a.Project,
                                  ShortName = a.Acronyme,
                                  ContractValue = a.ContractValue,
                                  Expenditure = (a.Rate * a.HoursWorked),
                                  ProjectMargin = ((a.ContractValue * 5) / 100),
                                  ProjectProfit = (a.ContractValue - (a.Rate * a.HoursWorked)),
                              };
            Details = QueryLevel2.OrderBy(r=>r.Project).ToList();
        }
    }
}
public class CostDetails
{
    public string Project { get; set; }
    public string ShortName { get; set; }
    public decimal ContractValue { get; set; }
    public decimal Expenditure { get; set; }
    public decimal ProjectMargin { get; set; }
    public decimal ProjectProfit { get; set; }
    public decimal Others { get; set; }
}
