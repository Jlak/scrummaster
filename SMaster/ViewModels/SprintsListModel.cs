﻿using PagedList;
using SMaster.Models.Setups;
using SMaster.Models.Sprints;
using SMaster.Services.Sprints;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.ViewModels
{
    public class SprintsListModel
    {
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }
        public int? Page { get; set; }
        [DisplayName("Project Name")]
        public int? ProjectId { get; set; }
        public IPagedList<PlannedSprintItem> Sprints { get; set; }
        public object GetRouteValues(int Page)
        {
            return new
            {
                Page,
                StartDate = StartDate?.ToString("yyyy-MM-dd"),
                EndDate = EndDate?.ToString("yyyy-MM-dd"),
            };
        }

        public void FilteredSprint(SprintPlanService _Sprintservice, int PageSize = 15)
        {
            var Query = _Sprintservice.GetPlannedsprints();

            if (StartDate == null)
            {
                StartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            }

            if (EndDate == null)
            {
                EndDate = StartDate.Value.AddDays(30);
            }

            if (StartDate.HasValue)
            {
                Query = Query.Where(r => r.StartDate >= StartDate);
            }

            if (EndDate.HasValue)
            {
                Query = Query.Where(r => r.EndDate <= EndDate);
            }

            if (ProjectId != null)
            {
                Query = Query.Where(r => r.ProjectId == ProjectId);
            }

            Sprints = (from a in Query.Select(r => new PlannedSprintItem
            {
                Project = r.Project.Name,
                Id = r.Id,
                EndDate = r.EndDate,
                StartDate = r.StartDate,
                IsDeletable = ((IQueryable<SprintTask>)r.Tasks).Any(SprintPlanService.UnDeletableTaskExpression),
                IsPending = r.Tasks.Any(t => t.Assignees.Any(a => a.TaskHistories.Any(h => h.Status < Enums.ActivityStatus.Done))),
                PMUsername = r.Project.Manager.UserName,
            }) orderby a.StartDate descending, a.Project ascending
                       select a
            ).ToPagedList(Page ?? 1, PageSize);
        }
        public class PlannedSprintItem
        {
            public int Id { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public bool IsDeletable { get; set; }
            public bool IsPending { get; set; }
            public string Project { get; set; }
            public string PMUsername { get; set; }
        }
    }
}