﻿using PagedList;
using SMaster.Entities;
using SMaster.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.ViewModels.Products
{
    public class ProjectListModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Page { get; set; }
        public string SearchTerm { get; set; }
        public IPagedList<Project> Projects { get; set; }
        public object GetRouteValues(int page)
        {
            return new
            {
                page,
                SearchTerm
            };
        }
        public void FilterProjects(ProductService _boardService, int PageSize = 10)
        {
            var baseQuery = _boardService.GetProjects();

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                baseQuery = baseQuery.Where(r => r.Name.Contains(SearchTerm));
            }

            if (StartDate != null)
            {
                baseQuery = baseQuery.Where(r => (r.StartDate.Value <= StartDate.Value));
            }

            if (EndDate != null)
            {
                baseQuery = baseQuery.Where(r => (r.EndDate.Value >= EndDate.Value));
            }

            Projects = baseQuery.OrderByDescending(r => new { r.StartDate }).ToPagedList(Page ?? 1, PageSize);
        }
    }
}
