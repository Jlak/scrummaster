﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Models
{
    public class DBSchemas
    {
        /// <summary>
        /// SCRUM Schema
        /// </summary>
        public const string SCRUM = "scrum";
        /// <summary>
        /// SECURITY Schema
        /// </summary>
        public const string SECURITY = "security";
        /// <summary>
        /// SECURITY Schema
        /// </summary>
        public const string SPRINTS = "sprints";
    }
}
