﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using SMaster.Interfaces;
using SMaster.Attributes;
using SMaster.Models.Setups;

namespace SMaster.Models.Security
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser, IErrorMessage
    {
        #region Fields

        [MaxLength(50), Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(50), Display(Name = "Last Name")]
        public string LastName { get; set; }
        [MaxLength(50), Display(Name = "Other Name")]
        public string OtherName { get; set; }
        [MaxLength(10)]
        public string Initials { get; set; }
        public string ProgramingLanguage { get; set; }
        [Display(Name = "Is User Active")]
        public bool IsActive { get; set; }
        public decimal? DailyRate { get; set; }

        #endregion

        #region Extra Logic

        /// <summary>
        /// NOT MAPPED
        /// </summary>
        [NotMapped]
        public string ErrorMessage { get; set; }
        /// <summary>
        /// NOT MAPPED: Used to initialize a new user's password
        /// </summary>
        [NotMapped, Display(Name = "Password"), UserPasswordValidation]
        public string Password { get; set; }

        [NotMapped, Display(Name = "Confirm Password"), UserPasswordValidation]
        public string ConfirmPassword { get; set; }
        /// <summary>
        /// NOT MAPPED: Whether or not it's a new User Account
        /// </summary>
        [NotMapped]
        public bool IsNewUser { get; set; }
        
        public string GetFullName
        {
            get
            {
                return (FirstName + " " + LastName + " " + OtherName);
            }
        }

        #endregion

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}