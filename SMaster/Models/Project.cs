using SMaster.Enums;
using SMaster.Interfaces;
using SMaster.Models;
using SMaster.Models.Security;
using SMaster.Models.Setups;
using SMaster.Models.Sprints;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SMaster.Entities
{
    [Table("Projects", Schema = DBSchemas.SCRUM)]
    public class Project : IAuditTrail, IErrorMessage
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(256), Display(Name = "Project Name")]
        public string Name { get; set; }
        [Required, StringLength(256), Display(Name ="Project Short Name")]
        public string ShortName { get; set; }
        [Required,Display(Name = "Project Amount")]
        public decimal Amount { get; set; }
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "Programing Platform")]
        public string ProgramingLanguage { get; set; }
        [StringLength(1024)]
        public string Description { get; set; }
        [ForeignKey("Manager"), Display(Name ="Project Manager")]
        public string ProjectManagerId { get; set; }
        [ForeignKey("Currency"), Display(Name = "Currency")] 
        public int? CurrencyId { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }
        public bool IsProjectClosed { get; set; }
        #region Audit Trail

        public DateTime? CreatedOn { get; set; }
        [ForeignKey("Creator")]
        public string CreatedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        [ForeignKey("LastModifier")]
        public string LastModifiedBy { get; set; }

        #endregion

        #region Navigation Properties 
        public virtual Currency Currency { get; set; }
        public virtual ApplicationUser Creator { get; set; }
        public virtual ApplicationUser LastModifier { get; set; }
        public virtual ApplicationUser Manager { get; set; }
        public virtual List<SprintPlan> SprintPlans { get; set; }
        public virtual List<ProjectClient> Clients { get; set; }
        public virtual List<ProjectExtension> Extentions { get; set; }
        #endregion

        public Project()
        {
            SprintPlans = new List<SprintPlan>();
            Clients = new List<ProjectClient>();
            Extentions = new List<ProjectExtension>();
        }
    }
}
