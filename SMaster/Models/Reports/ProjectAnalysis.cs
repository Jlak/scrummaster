﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SMaster.Models.Reports
{
    [Table("ProjectAnalysis", Schema = DBSchemas.SCRUM)]
    public class ProjectAnalysis
    {
        [Key]
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string ShortName { get; set; }
        public int ProjectId { get; set; }
        public decimal ProjectAllocatedHours { get; set; }
        public decimal? TotalRate { get; set; }
        public decimal HoursSpent { get; set; }
        public decimal HoursReminding
        {
            get
            {
                return (ProjectAllocatedHours - HoursSpent);
            }
        }
        public decimal ProjectBudget { get; set; }
        public decimal ActualFundSpent
        {
            get
            {
                return (ProjectBudget - (TotalRate.Value * HoursSpent));
            }
        }
        public decimal Balance
        {
            get
            {
                return (ProjectBudget - ActualFundSpent);
            }
        }
    }
}