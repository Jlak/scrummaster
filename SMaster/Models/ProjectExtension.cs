using SMaster.Enums;
using SMaster.Interfaces;
using SMaster.Models;
using SMaster.Models.Security;
using SMaster.Models.Setups;
using SMaster.Models.Sprints;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SMaster.Entities
{
    [Table("ProjectExtensions", Schema = DBSchemas.SCRUM)]
    public class ProjectExtension : IAuditTrail, INumericPrimaryKeyEntity
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Project"), Display(Name = "Project Name")]
        public int ProjectId { get; set; }
        [Required, Display(Name = "Reason For Extending")]
        public string Reason { get; set; }
        [Display(Name = "Project Amount")]
        public decimal? Amount { get; set; }
        [ForeignKey("Currency"), Display(Name = "Currency")]
        public int? CurrencyId { get; set; }
        public DateTime? EndDate { get; set; }
        #region Audit Trail

        public DateTime? CreatedOn { get; set; }
        [ForeignKey("Creator")]
        public string CreatedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        [ForeignKey("LastModifier")]
        public string LastModifiedBy { get; set; }

        #endregion

        #region Navigation Properties 
        public virtual Currency Currency { get; set; }
        public virtual Project Project { get; set; }

        public virtual ApplicationUser Creator { get; set; }
        public virtual ApplicationUser LastModifier { get; set; }

        #endregion
    }
}
