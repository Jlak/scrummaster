﻿using SMaster.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Models.Sprints
{
    [Table("SprintTasks", Schema = DBSchemas.SPRINTS)]
    public class SprintTask
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(255), Required]
        public string Description { get; set; }
        [Required, Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Required, Display(Name = "Planned Hours")]
        public decimal PlannedHours { get; set; }
        [ForeignKey("SprintPlan")]
        public int SprintPlanId { get; set; }
        public ActivityStatus Status { get; set; }
        public ActivityType Activitytype { get; set; }
        public ExtraActivityType? ExtraActivity { get; set; }
        public virtual SprintPlan SprintPlan { get; set; }
        public virtual List<SprintTaskAssignee> Assignees { get; set; }
        public SprintTask()
        {
            Assignees = new List<SprintTaskAssignee>();
        }

    }
}
