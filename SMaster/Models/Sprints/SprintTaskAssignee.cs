﻿using SMaster.Entities;
using SMaster.Enums;
using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Models.Sprints
{
    [Table("SprintTaskAssignees", Schema = DBSchemas.SPRINTS)]
    public class SprintTaskAssignee
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("User"), Index("IX_Unique", IsUnique = true, Order = 2)]
        public string UserId { get; set; }
        [ForeignKey("SprintTask"), Index("IX_Unique", IsUnique = true, Order = 1)]
        public int SprintTaskId { get; set; }
        public ActivityStatus Status { get; set; }
        public virtual SprintTask SprintTask { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual List<SprintTaskHistory> TaskHistories { get; set; }
        public SprintTaskAssignee()
        {
            TaskHistories = new List<SprintTaskHistory>();
        }
    }
}
