﻿using SMaster.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Models.Sprints
{
    [Table("SprintTaskHistories", Schema = DBSchemas.SPRINTS)]
    public class SprintTaskHistory
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Assignee"), Index("IX_Unique", IsUnique = true, Order = 1)]
        public int AssigneeId { get; set; }
        [Required, Index("IX_Unique", IsUnique = true, Order = 2)]
        public DateTime? ActivityDate { get; set; }
        public ActivityStatus Status { get; set; }
        public string Comment { get; set; }
        public DateTime? DateToComplete { get; set; }
        public decimal? ActualHours { get; set; }
        public virtual SprintTaskAssignee Assignee { get; set; }
    }
}
