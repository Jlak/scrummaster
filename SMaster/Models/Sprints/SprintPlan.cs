﻿using SMaster.Entities;
using SMaster.Enums;
using SMaster.Interfaces;
using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Models.Sprints
{
    [Table("SprintPlans", Schema = DBSchemas.SPRINTS)]
    public class SprintPlan : ICustomLookups
    {
        [Key]
        public int Id { get; set; }
        [Required, Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Required, Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        [Display(Name = "Client")]
        public int? ClientId { get; set; }
        public ActivityStatus Status { get; set; }
        [NotMapped]
        public ReportingType? selectedType { get; set; }
        [NotMapped]
        public int AbortSave { get; set; }
        #region Audit Trail

        public DateTime? CreatedOn { get; set; }
        [ForeignKey("Creator")]
        public string CreatedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        [ForeignKey("LastModifier")]
        public string LastModifiedBy { get; set; }

        #endregion

        #region Extra Logic

        [NotMapped]
        public string ErrorMessage { get; set; }

        #endregion

        #region Navigation Properties

        public virtual Project Project { get; set; }
        public virtual ApplicationUser Creator { get; set; }
        public virtual ApplicationUser LastModifier { get; set; }
        public virtual List<SprintTask> Tasks { get; set; }
        public SprintPlan()
        {
            Tasks = new List<SprintTask>();
        }

        #endregion
    }
}
