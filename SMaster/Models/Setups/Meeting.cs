﻿using SMaster.Interfaces;
using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SMaster.Models.Setups
{
    [Table("Meetings", Schema = DBSchemas.SCRUM)]
    public class Meeting : IAuditTrail, IErrorMessage
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Meeting Subject")]
        public string EventName { get; set; }
        [Display(Name = "Start Date/Time")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "End Date/Time")]
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        [Display(Name = "Metting Venue")]
        public string Venue { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }

        #region Audit Trail

        public DateTime? CreatedOn { get; set; }
        [ForeignKey("Creator")]
        public string CreatedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        [ForeignKey("LastModifier")]
        public string LastModifiedBy { get; set; }

        #endregion

        #region Navigation Properties 
        public virtual List<MeetingAttendee> Attendees { get; set; }
        public virtual ApplicationUser Creator { get; set; }
        public virtual ApplicationUser LastModifier { get; set; }

        #endregion

        public Meeting()
        {
            Attendees = new List<MeetingAttendee>();
        } 
    }
}