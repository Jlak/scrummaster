﻿using SMaster.Interfaces;
using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SMaster.Models.Setups
{
    [Table("MeetingAttendees", Schema = DBSchemas.SCRUM)]
    public class MeetingAttendee : INumericPrimaryKeyEntity
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Meeting")]
        public int MeetingId { get; set; }
        [ForeignKey("Appuser")]
        public string UserId { get; set; }
        [NotMapped]
        public string FullName { get; set; }
        public virtual ApplicationUser Appuser { get; set; }
        public virtual Meeting Meeting { get; set; }
    }
}
