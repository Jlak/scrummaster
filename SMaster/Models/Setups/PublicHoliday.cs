﻿using SMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SMaster.Models.Setups
{
    [Table("PublicHolidays", Schema = DBSchemas.SCRUM)]
    public class PublicHoliday : INumericPrimaryKeyEntity, IErrorMessage
    {
        [Key]
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }
    }
}