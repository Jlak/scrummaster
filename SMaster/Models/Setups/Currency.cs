﻿using SMaster.Entities;
using SMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Models.Setups
{
    [Table("Currencies", Schema = DBSchemas.SCRUM)]
    public class Currency : INumericPrimaryKeyEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
