﻿using SMaster.Entities;
using SMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Models.Setups
{
    [Table("ProjectClients", Schema = DBSchemas.SCRUM)]
    public class ProjectClient : INumericPrimaryKeyEntity
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Project")]
        public int? ProjectId { get; set; }
        public string Name { get; set; }
        public virtual Project Project { get; set; }
    }
}
