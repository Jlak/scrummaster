﻿using Microsoft.AspNet.Identity.EntityFramework;
using SMaster.Entities;
using SMaster.Models.Reports;
using SMaster.Models.Security;
using SMaster.Models.Setups;
using SMaster.Models.Sprints;
using System.Data.Entity;

namespace SMaster.Models
{
    public class Repository : IdentityDbContext<ApplicationUser>
    {
        public Repository() : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        #region Tables
        
        public DbSet<Project> Projects { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<MeetingAttendee> MeetingAttendees { get; set; }
        public DbSet<ProjectClient> Clients { get; set; }
        public DbSet<SprintTask> SprintTasks { get; set; }
        public DbSet<SprintTaskHistory> SprintTaskHistories { get; set; }
        public DbSet<SprintTaskAssignee> SprintTaskAssignees { get; set; }
        public DbSet<SprintPlan> SprintPlans { get; set; }
        public DbSet<ProjectAnalysis> ProjectAnalysis { get; set; }
        public DbSet<PublicHoliday> PublicHolidays { get; set; }

        #endregion

        public static Repository Create()
        {
            return new Repository();
        }

        public static Repository Create(bool EnableChangeTracking)
        {
            var database = new Repository();

            if (!EnableChangeTracking)
            {
                database.Configuration.AutoDetectChangesEnabled = false;
            }

            return database;
        }
    }
}