﻿using Hangfire;
using Microsoft.Owin;
using NDSLD.HangFire;
using Owin;
using SMaster.Services;

[assembly: OwinStartupAttribute(typeof(SMaster.Startup))]
namespace SMaster
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

            //Authenticate users who can view the dashboard 
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthenticationFilter() }
            });

            app.UseHangfireServer();

            RecurringJob.AddOrUpdate<NotificationService>("send-notifications", r => r.HangfireNotification(), Cron.DayInterval(2));
        }
    }
}
