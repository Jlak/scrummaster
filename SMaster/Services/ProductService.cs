﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMaster.Entities;
using SMaster.Models;


namespace SMaster.Services
{
    public class ProductService
    {
        private Repository database;
        public ProductService()
        {
            database = new Repository();
        }
        #region Projections
        public IQueryable<Project> GetProjects()
        {
            return database.Projects;
        }

        public bool SaveProject(Project project)
        {
            try
            {
                using (Repository database = new Repository())
                {
                    
                    if (project.Id == 0)//New Record
                    {
                        //Validate projection
                        if (database.Projects.Where(r => r.Name == project.Name).Count() > 0)
                        {
                            throw new Exception("Another Project with the same already exists!");
                        }

                        UtilityService.InitializeAuditTrail(database, project, true);

                        database.Projects.Add(project);
                    }
                    else//Existing record
                    {
                        var dbProject = database.Projects.Find(project.Id);

                        UtilityService.InitializeAuditTrail(database, project, false, dbProject);

                        foreach(var endDate in project.Extentions.Select(r=>r.EndDate))
                        {
                            if(endDate > project.EndDate)
                            {
                                project.IsProjectClosed = false;
                            }
                        }

                        database.Entry(dbProject).CurrentValues.SetValues(project);

                        project.Clients.ForEach(r => r.ProjectId = project.Id);
                        project.Extentions.ForEach(r => r.ProjectId = project.Id);

                        UtilityService.UpdateCollection(database, dbProject.Clients, project.Clients);
                        UtilityService.UpdateCollection(database, dbProject.Extentions, project.Extentions);
                    }

                    database.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                project.ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }
            return false;
        }

        public void Delete(int Id, out string ErrorMessage)
        {
            ErrorMessage = null;

            try
            {
                using (Repository database = new Repository())
                {
                    var dbProduct = database.Projects.Find(Id);
                    
                    database.Entry(dbProduct).State = System.Data.Entity.EntityState.Deleted;

                    database.SaveChanges();
                }
            }
            catch (Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }
        }
        #endregion
    }
}