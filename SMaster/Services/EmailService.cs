﻿using SMaster.Models.Sprints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Services
{
    public class EmailService
    {
        //public void SendEmail(string SmtpHost, string SenderEmailAddress, string SenderPassword, int SmtpPort, SprintTaskHistory model)
        public void SendEmail(SprintTaskHistory model)
        {

            string SmtpHost = "mail.dcareug.com";
            string SenderEmailAddress = "support@dcareug.com";
            string SenderPassword = "d@t@c@r3";
            int SmtpPort = 587;
            try
            {
                using (SmtpClient SmtpServer = new SmtpClient(SmtpHost))
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(SenderEmailAddress);

                        mail.To.Add(model.Assignee.SprintTask.SprintPlan.Project.Manager.Email);

                        mail.Subject = "Report from " + model.Assignee.User.FirstName + model.Assignee.User.LastName + model.Assignee.User.OtherName;
                        mail.Body = "Dear Manager, the following task has been completed " + model.Assignee.SprintTask.Description + " on : " + DateTime.Now;
                        mail.IsBodyHtml = true;

                        SmtpServer.Port = SmtpPort;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.Credentials = new System.Net.NetworkCredential(SenderEmailAddress, SenderPassword);
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Timeout = 120000;
                        SmtpServer.Send(mail);
                    }
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}
