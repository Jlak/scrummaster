﻿using SMaster.Helpers;
using SMaster.Interfaces;
using SMaster.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace SMaster.Services
{
    /// <summary>
    /// Provides extension methods for Services
    /// </summary>
    public static class UtilityService
    {
        /// <summary>
        /// Finds the inner-most exception message and returns it
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string ExtractInnerExceptionMessage(Exception ex)
        {
            if (ex.InnerException != null)
            {
                return ExtractInnerExceptionMessage(ex.InnerException);
            }

            return ex.Message;
        }

        /// <summary>
        /// Initializes the Audit Trail fields for a given record
        /// </summary>
        /// <param name="Record"></param>
        /// <param name="IsANewRecord"></param>
        /// <param name="PreviousVersionOfRecord">From which to copy creation data for preservation</param>
        public static void InitializeAuditTrail(Repository database, IAuditTrail Record, bool IsANewRecord, IAuditTrail PreviousVersionOfRecord = null)
        {
            var CurrentUserId = SMasterUserManager.GetUserManager(database).FindByName(HttpContext.Current.User.Identity.Name).Id;

            if (IsANewRecord)//New Record
            {
                Record.CreatedBy = CurrentUserId;
                Record.CreatedOn = DateTime.Now;
            }
            else
            {
                if (PreviousVersionOfRecord != null)
                {
                    Record.CreatedBy = PreviousVersionOfRecord.CreatedBy;
                    Record.CreatedOn = PreviousVersionOfRecord.CreatedOn;
                }

                Record.LastModifiedBy = CurrentUserId;
                Record.LastModifiedOn = DateTime.Now;
            }
        }

        /// <summary>
        /// Syncronises the Original List to match the Updated List of type IURCEntity. Adds new items to the DbSet if required.
        /// The Foreign Key fields should already be set for each UpdatedList item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="database"></param>
        /// <param name="OriginalList">The Original Database List.</param>
        /// <param name="UpdatedList">The Updated List. The Foreign Key fields should already be set for each item</param>
        /// <param name="ModelToCopyAuditTrailFrom">If supplied, it's assumed the collection implements IAuditTrail and it inherits this object's audit trail.</param>
        public static void SynchronizeCollections<T>(DbContext database, List<T> OriginalList, List<T> UpdatedList, IAuditTrail ModelToCopyAuditTrailFrom = null) where T : class, INumericPrimaryKeyEntity
        {
            var newListIds = UpdatedList.Select(r => r.Id).ToList();

            //Delete removed entries
            foreach (var entry in OriginalList.Where(a => newListIds.Contains(a.Id) == false).ToList())
            {
                database.Entry(entry).State = System.Data.Entity.EntityState.Deleted;
            }

            //Update existing
            foreach (var entry in OriginalList.Where(r => newListIds.Contains(r.Id)).ToList())
            {
                var updatedEntry = UpdatedList.Where(r => r.Id == entry.Id).Single();

                if (ModelToCopyAuditTrailFrom != null)//Audit Trail enabled
                {
                    //Copy creation
                    CopyAuditTrailToCollection(new[] { (IAuditTrail)updatedEntry }, (IAuditTrail)entry, true);

                    //Copy modification
                    CopyAuditTrailToCollection(new[] { (IAuditTrail)updatedEntry }, ModelToCopyAuditTrailFrom, false, false);
                }

                database.Entry(entry).CurrentValues.SetValues(updatedEntry);
            }

            //Add new entries

            foreach (var entry in UpdatedList.Where(r => r.Id == 0).ToList())
            {
                if (ModelToCopyAuditTrailFrom != null)
                {
                    CopyAuditTrailToCollection(new[] { (IAuditTrail)entry }, ModelToCopyAuditTrailFrom, true);
                }
                database.Set<T>().Add(entry);
            }
        }

        /// <summary>
        /// Syncronises the Original List to match the Updated List of type IURCEntity. Adds new items to the DbSet if required.
        /// The Foreign Key fields should already be set for each UpdatedList item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="database"></param>
        /// <param name="OriginalList">The Original Database List.</param>
        /// <param name="UpdatedList">The Updated List. The Foreign Key fields should already be set for each item</param>
        public static void SynchronizeCollectionsByValue<T>(DbContext database, IEnumerable<T> OriginalList, IEnumerable<T> UpdatedList) where T : class
        {
            //Delete removed entries
            foreach (var entry in OriginalList.Where(a => !UpdatedList.Contains(a)).ToList())
            {
                database.Entry(entry).State = System.Data.Entity.EntityState.Deleted;
            }

            //Update existing
            foreach (var entry in OriginalList.Where(r => UpdatedList.Contains(r)).ToList())
            {
                var updatedEntry = UpdatedList.Where(r => UpdatedList.Contains(r)).Single();

                database.Entry(entry).CurrentValues.SetValues(updatedEntry);
            }

            //Add new entries

            foreach (var entry in UpdatedList.Where(r => !OriginalList.Contains(r)).ToList())
            {
                database.Set<T>().Add(entry);
            }
        }

        /// <summary>
        /// Copies AuditTrail information to a collection from a given IAuditTrail model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="RecordList"></param>
        /// <param name="ModelToCopy"></param>
        /// <param name="CopyCreationTrail">true = copy the creation trail, false = copy last modified trail</param>
        /// <param name="IntoCreationFields">true = copy into creation fields, false = copy into last modified fields</param>
        public static void CopyAuditTrailToCollection<T>(IEnumerable<T> RecordList, IAuditTrail ModelToCopy, bool CopyCreationTrail, bool IntoCreationFields = true) where T : class, IAuditTrail
        {
            foreach (var entry in RecordList)
            {
                if (IntoCreationFields)
                {
                    entry.CreatedBy = CopyCreationTrail ? ModelToCopy.CreatedBy : ModelToCopy.LastModifiedBy;
                    entry.CreatedOn = CopyCreationTrail ? ModelToCopy.CreatedOn : ModelToCopy.LastModifiedOn;
                }
                else
                {
                    entry.LastModifiedBy = CopyCreationTrail ? ModelToCopy.CreatedBy : ModelToCopy.LastModifiedBy;
                    entry.LastModifiedOn = CopyCreationTrail ? ModelToCopy.CreatedOn : ModelToCopy.LastModifiedOn;
                }
            }
        }

        /// <summary>
        /// Exports the contents of a partial view to excel.
        /// </summary>
        /// <param name="Response">The HttpResponseBase to override.</param>
        /// <param name="partial">The PartialViewResult to export.</param>
        /// <param name="Filename">The name of the file generated.</param>
        /// <returns></returns>
        public static ActionResult ExportPartialToExcel(HttpResponseBase Response, PartialViewResult partial, string Filename)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename.Replace(" ", "_") + ".xls");
            Response.ContentType = "application/ms-excel";
            FileInfo fi = new FileInfo("../Content/Styles/StyleSheet.css");

            return partial;
        }

        /// <summary>
        /// Syncronises the Original List to match the Updated List of type IURCEntity. Adds new items to the DbSet if required.
        /// The Foreign Key fields should already be set for each UpdatedList item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="database"></param>
        /// <param name="OriginalList">The Original Database List.</param>
        /// <param name="UpdatedList">The Updated List. The Foreign Key fields should already be set for each item</param>
        public static void UpdateCollection<T>(DbContext database, List<T> OriginalList, List<T> UpdatedList) where T : class, INumericPrimaryKeyEntity
        {
            var newListIds = UpdatedList.Select(r => r.Id).ToList();

            //Delete removed entries
            foreach (var entry in OriginalList.Where(a => newListIds.Contains(a.Id) == false).ToList())
            {
                database.Entry(entry).State = System.Data.Entity.EntityState.Deleted;
            }

            //Update existing
            foreach (var entry in OriginalList.Where(r => newListIds.Contains(r.Id)).ToList())
            {
                database.Entry(entry).CurrentValues.SetValues(UpdatedList.Where(r => r.Id == entry.Id).Single());
            }

            //Add new entries
            foreach (var entry in UpdatedList.Where(r => r.Id == 0).ToList())
            {
                database.Set<T>().Add(entry);
            }
        }
        
        public static IEnumerable<string> GetSearchTerms(string SearchTerm)
        {
            return string.IsNullOrEmpty(SearchTerm) ? new List<string>() : SearchTerm.Split(',').ToList();
        }

        /// <summary>
        /// Shortens a string Value to use as a SearchTerm in URL
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static string SanitizeAsSearchTerm(string Value)
        {
            return (Value?.Length ?? 0) > 30 ? Value.Substring(0, 30) : Value;
        }
    }
}
