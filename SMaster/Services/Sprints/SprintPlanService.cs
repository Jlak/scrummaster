﻿using FSDU.Helpers;
using SMaster.Entities;
using SMaster.Enums;
using SMaster.Models;
using SMaster.Models.Reports;
using SMaster.Models.Security;
using SMaster.Models.Setups;
using SMaster.Models.Sprints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SMaster.Services.Sprints
{
    public class SprintPlanService
    {
        private Repository database;
        private EmailService emailService;
        public SprintPlanService()
        {
            database = new Repository();
            emailService = new EmailService();
        }

        public Repository GetDatabase()
        {
            return database;
        }

        public IQueryable<SprintPlan> GetPlannedsprints()
        {
            return database.SprintPlans;
        }

        public IQueryable<Project> GetProjects()
        {
            return database.Projects;
        }

        public IQueryable<ProjectAnalysis> GetProjectAnalysis()
        {
            return database.ProjectAnalysis;
        }

        public IQueryable<SprintTask> GetPlannedTasks()
        {
            return database.SprintTasks;
        }
        public IQueryable<SprintTaskHistory> GetPlannedTaskHistories()
        {
            return database.SprintTaskHistories;
        }

        public IQueryable<SprintTaskAssignee> GetAssignees()
        {
            return database.SprintTaskAssignees;
        }


        public IQueryable<ApplicationUser> GetUsers()
        {
            return database.Users.Where(r=>r.IsActive);
        }
        public bool SaveSprintPlan(SprintPlan plan)
        {
            try
            {
                //Validation
                if (plan.Tasks.Count == 0)
                {
                    throw new Exception("A Sprint must have atleast 1 task!");
                }
                else if (plan.Tasks.Any(r => r.Assignees.Count == 0))
                {
                    throw new Exception("Every Activity must be assigned to atleast 1 person!");
                }

                using(TransactionScope scope=new TransactionScope())
                {
                    using (Repository database = new Repository())
                    {
                        var dbPlan = database.SprintPlans.Find(plan.Id);

                        if (dbPlan == null)
                        {
                            dbPlan = plan;

                            CreateSprintPlan(database, dbPlan, plan.selectedType);
                        }
                        else
                        {
                            EditSprintPlan(database, dbPlan, plan, plan.selectedType);
                        }
                        database.SaveChanges();

                        UpdateSprintPlanStatuses(database, dbPlan);
                    }
                    scope.Complete();
                }
                return true;
            }

            catch (Exception e)
            {
                plan.ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        private void CreateSprintPlan(Repository database, SprintPlan plan, ReportingType? selectedType)
        {

            if (plan.selectedType == ReportingType.ExtraActivity)
            {
                var dates = new List<DateTime>();
                for (var activitydate = plan.StartDate; activitydate <= plan.EndDate; activitydate = activitydate.Value.AddDays(1))
                {
                    if (activitydate.Value.DayOfWeek == DayOfWeek.Sunday || activitydate.Value.DayOfWeek == DayOfWeek.Saturday)
                    {

                    }
                    else
                    {
                        plan.Tasks.ForEach(task =>
                        {
                            task.Assignees.ForEach(assignee =>
                            {
                                task.StartDate = activitydate;
                                AddAssigneeTaskHistory(task, assignee, selectedType);
                            });
                        });
                    }
                }
            }
            else
            {
                plan.Tasks.ForEach(task =>
                {
                    task.Assignees.ForEach(assignee =>
                    {
                        AddAssigneeTaskHistory(task, assignee, selectedType);
                    });
                });
            }
            UtilityService.InitializeAuditTrail(database, plan, true);

            database.SprintPlans.Add(plan);
        }

        private void AddAssigneeTaskHistory(SprintTask task,SprintTaskAssignee assignee, ReportingType? selectedType)
        {
            if (selectedType == ReportingType.ExtraActivity)
            {
                assignee.TaskHistories.Add(new SprintTaskHistory
                {
                    ActivityDate = task.StartDate,
                    ActualHours = 8,
                    Status = Enums.ActivityStatus.Done,
                });
            }
            else
            {
                assignee.TaskHistories.Add(new SprintTaskHistory
                {
                    ActivityDate = task.StartDate,
                    Status = Enums.ActivityStatus.Pending,
                });
            }
        }

        private void EditSprintPlan(Repository database, SprintPlan dbPlan, SprintPlan UpdatedPlan, ReportingType? selectedType)
        {
            #region Manage Tasks

            var ExistingTaskIds = dbPlan.Tasks.Select(r => r.Id).ToList();

            //Update and Add Tasks
            foreach (var updatedtask in UpdatedPlan.Tasks)
            {
                if (updatedtask.Id==0)//New Task
                {
                    var dbTask = updatedtask;

                    dbTask.SprintPlanId = dbPlan.Id;

                    dbTask.Assignees.ForEach(assignee => {
                        AddAssigneeTaskHistory(dbTask, assignee, selectedType);
                    });

                    database.SprintTasks.Add(dbTask);
                }
                else//Existing
                {
                    var dbTask = dbPlan.Tasks.Where(r => r.Id == updatedtask.Id).Single();

                    ExistingTaskIds.Remove(updatedtask.Id);

                    database.Entry(dbTask).CurrentValues.SetValues(updatedtask);

                    #region Manage Assignees

                    var existingAssigneeIds = dbTask.Assignees.Select(r => r.Id).ToList();

                    //Update and Add New
                    foreach (var updatedAssignee in updatedtask.Assignees)
                    {
                        var dbAssignee = dbTask.Assignees.Where(r => r.UserId == updatedAssignee.UserId).SingleOrDefault();

                        if (dbAssignee == null)//new
                        {
                            dbAssignee = updatedAssignee;
                            dbAssignee.SprintTaskId = dbTask.Id;

                            AddAssigneeTaskHistory(dbTask, dbAssignee, selectedType);

                            database.SprintTaskAssignees.Add(dbAssignee);
                        }
                        else//existing
                        {
                            if (dbAssignee.TaskHistories.Count <= 1)
                            {
                                dbAssignee.TaskHistories.ForEach(r => {
                                    if(r.Status== ActivityStatus.Pending)
                                    {
                                        r.ActivityDate = dbTask.StartDate;
                                    }
                                });
                            }

                            existingAssigneeIds.Remove(dbAssignee.Id);
                        }
                    }
                    //Delete Removed Assignees
                    foreach (var Id in existingAssigneeIds)
                    {
                        var dbAssignee = dbTask.Assignees.Where(r => r.Id == Id).Single();

                        //only delete if all histories are in pending
                        if (!database.SprintTaskHistories.Where(r => r.AssigneeId == Id).Any(UnDeletableTaskHistoryExpression))
                        {
                            database.Entry(dbAssignee).State = System.Data.Entity.EntityState.Deleted;
                        }
                    }

                    #endregion
                }
            }

            //Delete removed Tasks
            foreach (var Id in ExistingTaskIds)
            {
                var dbTask = dbPlan.Tasks.Where(r => r.Id == Id).Single();

                //Task has no assignee with history other than Pending
                if (!database.SprintTaskAssignees.Where(r=>r.SprintTaskId==Id).Any(UnDeletableTaskAssigneeExpression))
                {
                    database.Entry(dbTask).State = System.Data.Entity.EntityState.Deleted;
                }
            }

            #endregion

            UtilityService.InitializeAuditTrail(database, UpdatedPlan, false, dbPlan);

            database.Entry(dbPlan).CurrentValues.SetValues(UpdatedPlan);
        }

        private static ActivityStatus[] DeletableStatuses = new[] { ActivityStatus.Pending, ActivityStatus.Cancelled};

        private static Expression<Func<SprintTaskHistory, bool>> UnDeletableTaskHistoryExpression = r => !DeletableStatuses.Contains(r.Status);

        private static Expression<Func<SprintTaskAssignee, bool>> UnDeletableTaskAssigneeExpression = r => 
        ((IQueryable<SprintTaskHistory>)r.TaskHistories).Any(UnDeletableTaskHistoryExpression);

        public static Expression<Func<SprintTask, bool>> UnDeletableTaskExpression = r =>
            ((IQueryable<SprintTaskAssignee>)r.Assignees).Any(UnDeletableTaskAssigneeExpression);

        public bool DeleteSprintPlan(int Id,out string ErrorMessage)
        {
            ErrorMessage = null;

            try
            {
                using(var database=new Repository())
                {
                    var dbPlan = database.SprintPlans.Find(Id);

                    if(database.SprintTasks.Where(r=>r.SprintPlanId==Id).Any(UnDeletableTaskExpression))
                    {
                        throw new Exception("This sprint CAN'T be deleted!");
                    }
                    else
                    {
                        database.Entry(dbPlan).State = System.Data.Entity.EntityState.Deleted;
                    }

                    database.SaveChanges();
                }

                return true;
            }
            catch(Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        private static ActivityStatus[] RequireDateToCompleteStatuses = new[] { ActivityStatus.Ongoing, ActivityStatus.Pending };
        private static ActivityStatus[] RequireActualHours = new[] { ActivityStatus.Ongoing, ActivityStatus.Done };

        public bool SaveDailyTask(SprintTaskHistory item, out string ErrorMessage, string Username)
        {
            ErrorMessage = null;

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var database = Repository.Create())
                    {
                        SprintTaskHistory dbTaskHistory = database.SprintTaskHistories.Where(r => r.Id == item.Id).Single();

                        if (!RequireDateToCompleteStatuses.Contains(dbTaskHistory.Status))
                        {
                            throw new Exception("This task was already "+dbTaskHistory.Status+"!");
                        }

                        var dbAssignee = dbTaskHistory.Assignee;

                        if (RequireDateToCompleteStatuses.Contains(item.Status) && item.DateToComplete == null)
                        {
                            throw new Exception("Please select the date you plan to complete all the pending tasks!");
                        }

                        if (item.ActualHours == null && RequireActualHours.Contains(item.Status))
                        {
                            throw new Exception("Please enter the number of hours you spent on this activity!");
                        }

                        //update task
                        dbAssignee.Status = item.Status;

                        if (RequireDateToCompleteStatuses.Contains(item.Status))
                        {
                            dbTaskHistory.DateToComplete = item.DateToComplete;
                            AddTaskHistory(dbAssignee, item.DateToComplete);
                        }

                        //update history
                        dbTaskHistory.ActualHours = item.ActualHours;
                        dbTaskHistory.Status = item.Status;
                        dbTaskHistory.Comment = item.Comment;

                        database.SaveChanges();

                        UpdateSprintPlanStatuses(database, dbAssignee.SprintTask.SprintPlan);

                        if (item.Status == ActivityStatus.Done || item.Status == ActivityStatus.Cancelled)
                        {
                            emailService.SendEmail(item);
                        }
                    }
                    scope.Complete();

                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        private void AddTaskHistory(SprintTaskAssignee dbAssignee, DateTime? TargetDate)
        {
            var history = new SprintTaskHistory
            {
                Status = dbAssignee.Status,
                ActivityDate = TargetDate,
            };

            dbAssignee.TaskHistories.Add(history);
        }

        /// <summary>
        /// Should execute within a TransactionScope
        /// </summary>
        /// <param name="database"></param>
        /// <param name="dbPlan"></param>
        private void UpdateSprintPlanStatuses(Repository database,SprintPlan dbPlan)
        {

            dbPlan.Tasks.ForEach(task=> {
                task.Status = task.Assignees.Min(r => r.Status);
            });

            dbPlan.Status = dbPlan.Tasks.Min(r => r.Status);

            database.SaveChanges();
        }
        
    }
}
