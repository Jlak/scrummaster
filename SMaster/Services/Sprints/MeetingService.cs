﻿using SMaster.Enums;
using SMaster.Models;
using SMaster.Models.Setups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace SMaster.Services.Sprints
{
    public static class MeetingService
    {

        public static IQueryable<Meeting> GetMeetings(Repository database)
        {
            return database.Meetings;
        }

        public static IQueryable<PublicHoliday> GetHolidays(Repository database)
        {
            return database.PublicHolidays;
        }
        public static List<string> Save(Meeting meeting)
        {
            List<string> validationErrors = new List<string>();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (Repository database = new Repository())
                    {
                        #region Validations

                        var existingMeeting = database.MeetingAttendees.ToList();
                        var newAttendees = meeting.Attendees.Select(r => r.UserId).ToList();

                        foreach (var existing in existingMeeting)
                        {
                            if (newAttendees.ToList().Contains(existing.UserId))
                            {
                                if (existingMeeting.Select(r => r.Meeting.StartDate).ToList().Contains(meeting.StartDate.Value))
                                {
                                    foreach (var time in existingMeeting.Select(r => r.Meeting.EndDate < meeting.StartDate))
                                    {
                                        validationErrors.Add(existing.Appuser.FirstName + " " + existing.Appuser.LastName + " " + existing.Appuser.OtherName + "is booked for another meeting at this time");
                                    }
                                }
                            }
                        }
                        
                        if(validationErrors.Count() > 0)
                        {
                            throw new Exception();
                        }

                        //Validate Attendees of the meeting
                        if (meeting.Attendees.Count() == 0)
                        {
                            throw new Exception("A Meeting must have atleast 1 person attending!");
                        }

                        #endregion

                        if (meeting.Id == 0)//New Record
                        {
                            UtilityService.InitializeAuditTrail(database, meeting, true);

                            database.Meetings.Add(meeting);
                        }
                        else//Existing record
                        {
                            var dbMeeting = database.Meetings.Find(meeting.Id);

                            UtilityService.InitializeAuditTrail(database, meeting, false, dbMeeting);

                            database.Entry(dbMeeting).CurrentValues.SetValues(meeting);

                            meeting.Attendees.ForEach(r => r.MeetingId = meeting.Id);

                            UtilityService.UpdateCollection(database, dbMeeting.Attendees, meeting.Attendees);
                        }

                        database.SaveChanges();
                    }
                    scope.Complete();

                    return validationErrors;
                }
            }
            catch (Exception e)
            {
                validationErrors.Add(UtilityService.ExtractInnerExceptionMessage(e));
            }
            return validationErrors;
        }

        public static bool DeleteRecord(int Id, out string ErrorMessage)
        {
            ErrorMessage = null;

            try
            {
                using (var database = new Repository())
                {
                    var dbMeeting = database.Meetings.Find(Id);

                    database.Entry(dbMeeting).State = System.Data.Entity.EntityState.Deleted;

                    database.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        public static List<string> SaveHoliday(PublicHoliday model)
        {
            List<string> validationErrors = new List<string>();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (Repository database = new Repository())
                    {
                        if (model.Id == 0)//New Record
                        {
                            database.PublicHolidays.Add(model);
                        }
                        else//Existing record
                        {
                            var dbMeeting = database.PublicHolidays.Find(model.Id);
                            database.Entry(dbMeeting).CurrentValues.SetValues(model);
                        }

                        database.SaveChanges();
                    }
                    scope.Complete();

                    return validationErrors;
                }
            }
            catch (Exception e)
            {
                validationErrors.Add(UtilityService.ExtractInnerExceptionMessage(e));
            }
            return validationErrors;
        }
    }
}
