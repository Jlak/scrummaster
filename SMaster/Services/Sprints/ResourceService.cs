﻿using SMaster.Entities;
using SMaster.Models;
using SMaster.Models.Reports;
using SMaster.Models.Sprints;
using System;
using System.Linq;
using System.Transactions;

namespace SMaster.Services.Sprints
{
    public static class ResourceService
    {
        public static bool SaveRecords(out string ErrorMessage, string Username)
        {
            ErrorMessage = null;

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var database = Repository.Create())
                    {
                        ///Delete the existing data 
                        DeleteAnalysisRecords(database);

                        var projectsData = database.Projects.ToList();

                        ///Adding the new records in the analysis
                        foreach (var item in projectsData)
                        {
                            SaveAnalysis(database, item);
                        }

                        database.SaveChanges();
                    }
                    scope.Complete();

                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        public static bool DeleteAnalysisRecords(Repository database)
        {
            try
            {
                var dbPlan = database.ProjectAnalysis;

                database.Database.ExecuteSqlCommand("DELETE FROM scrum.ProjectAnalysis");

                database.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
               string ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        public static bool SaveAnalysis(Repository database, Project currentProject)
        {

            try
            {
                ProjectAnalysis dbAnalysis = new ProjectAnalysis();
                
                    dbAnalysis.ProjectName = currentProject.Name;
                    dbAnalysis.ShortName = currentProject.ShortName;
                    dbAnalysis.ProjectId = currentProject.Id;
                    dbAnalysis.ProjectAllocatedHours = currentProject.SprintPlans.Sum(r=>r.Tasks.Sum(m=>m.PlannedHours));
                    dbAnalysis.HoursSpent = currentProject.SprintPlans.Sum(r => r.Tasks.Sum(m => m.Assignees.Sum(b=>b.TaskHistories.Sum(t=>t.ActualHours.Value)))); 
                    dbAnalysis.ProjectBudget = currentProject.Amount;
                    dbAnalysis.TotalRate = currentProject.SprintPlans.Sum(r => r.Tasks.Sum(m => m.Assignees.Sum(b => b.User.DailyRate)));

                    database.ProjectAnalysis.Add(dbAnalysis);
            }
            catch (Exception e)
            {
               string ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }
    }
}