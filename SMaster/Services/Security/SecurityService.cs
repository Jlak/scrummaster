﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SMaster.Models;
using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Services.Security
{
    public class SecurityService
    {
        public Repository database;

        public SecurityService()
        {
            database = Repository.Create(false);
        }

        #region Manage Users

        /// <summary>
        /// Gets all Users
        /// </summary>
        /// <returns></returns>
        public IQueryable<ApplicationUser> GetUsers()
        {
            return database.Users;
        }

        public bool SaveUser(ApplicationUser appUser)
        {
            try
            {
                using (var database = Repository.Create())
                {
                    //Validate Username
                    if (database.Users.Where(r => r.UserName == appUser.UserName && r.Id != appUser.Id).Count() > 0)//Duplicate Username
                    {
                        throw new Exception("The UserName '" + appUser.UserName + "' already exists!");
                    }

                    var userManager = SMaster.Helpers.SMasterUserManager.GetUserManager(database);

                    //Remove null records
                    foreach (var item in appUser.Roles.Where(r => r.RoleId == null).ToList())
                    {
                        appUser.Roles.Remove(item);
                    }

                    if (appUser.IsNewUser)
                    {
                        (appUser.Roles).ToList().ForEach(r => r.UserId = appUser.Id);
                        //Attempt to create User Account
                        var result = userManager.Create(appUser, appUser.Password);

                        if (!result.Succeeded)
                        {
                            var errorsFound = "<ul>";

                            foreach (var error in result.Errors)
                            {
                                errorsFound += "<li>" + error + "</li>";
                            }

                            errorsFound += "</ul>";

                            throw new Exception(errorsFound);
                        }
                    }
                    else
                    {
                        var dbUser = database.Users.Find(appUser.Id);

                        foreach (var field in new[] { "FirstName", "LastName", "OtherName" })
                        {
                            dbUser.GetType().GetProperty(field).SetValue(dbUser, appUser.GetType().GetProperty(field).GetValue(appUser, null));
                        }

                        var userRoles = appUser.Roles.Where(r => r.RoleId != null).ToList();
                        userRoles.ForEach(r => r.UserId = appUser.Id);

                        UtilityService.SynchronizeCollectionsByValue<IdentityUserRole>(database, dbUser.Roles, userRoles);

                        database.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                appUser.ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        public bool ResetUserPassword(string UserId, string NewPassword, out string ErrorMessage)
        {
            ErrorMessage = null;

            try
            {
                if (string.IsNullOrEmpty(NewPassword))
                {
                    throw new Exception("The New Password is required!");
                }

                using (Repository database = new Repository())
                {
                    var userManager = SMaster.Helpers.SMasterUserManager.GetUserManager(database);

                    var resetToken = userManager.GeneratePasswordResetToken(UserId);

                    var result = userManager.ResetPassword(UserId, resetToken, NewPassword);

                    if (!result.Succeeded)
                    {
                        string Errors = "<ul>";

                        foreach (var error in result.Errors)
                        {
                            Errors += "<li>" + error + "</li>";
                        }

                        Errors += "</ul>";
                        throw new Exception(Errors);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        #endregion

        #region Roles

        public IQueryable<IdentityRole> GetRoles()
        {
            return database.Roles;
        }

        #endregion
    }
}
