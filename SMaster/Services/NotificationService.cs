﻿using SMaster.Enums;
using SMaster.Helpers;
using SMaster.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using static SMaster.ViewModels.Dashboard.DashboardListModel;

namespace SMaster.Services
{
    public class NotificationService
    {
        public bool HangfireNotification()
        {
            return PrepareNotification();
        }

        public bool PrepareNotification(DateTime? StartDate = null, DateTime? EndDate = null)
        {
            StartDate =(StartDate.HasValue ? StartDate : new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1));
            EndDate = (EndDate.HasValue ? EndDate : DateTime.Today);

            using (Repository database = new Repository())
            {
                var data = database.SprintTaskHistories;
                var TaskHistories = database.SprintTaskHistories.Where(r => r.ActivityDate >= StartDate && r.ActivityDate <= EndDate && r.Assignee.User.IsActive);

                var WorkingDays = GenericLookupHelper.GetNumberOfWorkingDays(StartDate.Value, EndDate.Value);

                var query = from a in TaskHistories
                            select new
                            {
                                UserId = a.Assignee.UserId,
                                UserName = a.Assignee.User.FirstName + " " + a.Assignee.User.LastName + " " + a.Assignee.User.OtherName,
                                ActualHours = a.ActualHours,
                                ActivityType = a.Assignee.SprintTask.ExtraActivity,
                                PlannedHours = a.Assignee.SprintTask.PlannedHours,
                                DayWorked = a.ActivityDate
                            };

                var QueryLevel2 = from a in query
                                  group a by new { a.UserName, a.UserId } into ResultSet
                                  select new
                                  {
                                      UserId = ResultSet.Key.UserId,
                                      StaffName = ResultSet.Key.UserName,
                                      ActualHours = ResultSet.Sum(r => r.ActualHours),
                                      LeaveHours = ResultSet.Where(r => r.ActivityType == ExtraActivityType.Leave).Sum(r => r.ActualHours),
                                      PublicDays = ResultSet.Where(r => r.ActivityType == ExtraActivityType.Public_Holiday).Sum(r => r.ActualHours),
                                      PlannedHours = ResultSet.Sum(r => r.PlannedHours),
                                      DaysWorked = (ResultSet.Select(r => r.DayWorked).Distinct().Count()),
                                  };

                var finalResult = from a in QueryLevel2
                             select new UsersLevelOfEffort
                             {
                                 UserName = a.StaffName,
                                 ActualHours = a.ActualHours,
                                 LeaveHours = (a.LeaveHours),
                                 PublicDays = (a.PublicDays),
                                 PlannedHours = a.PlannedHours,
                                 ExpectedHours = WorkingDays * 8,
                                 LeaveTime = a.ActualHours,
                                 UserId = a.UserId,
                                 PercentageWorked = ((a.ActualHours == null ? 0 : a.ActualHours / (WorkingDays * 8)) * 100),
                             };

                if (finalResult.Count() > 0)
                {
                    return SendNotification(database, finalResult.OrderBy(r => r.UserName).ToList(), StartDate.Value, EndDate.Value, true);
                }
                return true;
            }
        }
        
        public static string UsersToNotify = System.Web.Configuration.WebConfigurationManager.AppSettings["UsersToNotify"].ToString();
        public static string SmtpHost = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpHost"].ToString();
        public static string SmtpEmailAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpEmailAddress"].ToString();
        public static string SmtpPort = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpPort"].ToString();
        public static string SmtpEmailPassword = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpEmailPassword"].ToString();
        public static string EnableSsl = System.Web.Configuration.WebConfigurationManager.AppSettings["EnableSsl"].ToString();
        
        /// <summary>
        /// Send Notification email after receiving all the required values 
        /// </summary>
        /// <param name="partners"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public bool SendNotification(Repository _repository, List<UsersLevelOfEffort> levels, DateTime StartDate, DateTime EndDate, bool IsSystemGenerated)
        {
            try
            {
                using (SmtpClient SmtpServer = new SmtpClient(SmtpHost))
                {
                    List<string> Emails = new List<string>();
                    foreach (var user in UsersToNotify.Split(';'))
                    {
                        Emails.Add(user);
                    }

                    foreach (var address in Emails)
                    {
                        using (MailMessage mail = new MailMessage())
                        {
                            mail.To.Add(address);
                            mail.From = new MailAddress(SmtpEmailAddress);

                            mail.Subject = "ScrumMaster Reporting Status";

                            var textBody = "<b>Dear Sir/Madam <br /> Find below the scrummaster reporting status from " + StartDate.FormatForDisplay() + " to " + EndDate.FormatForDisplay() + " check the table below for details</b> <br /><table cellspacing='0' style='font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif' border=" + 1 + "><thead>" +
                                         "<tr style='background-color:#5cb85c'><th>User Full Name </th><td align='right'><b> Total Hours Expected </b></td><th> Leave Hours</th><th>Public Holiday</th><th>Planned Hours</th><th>Hours Logged</th><th>Percentage %</th></tr></thead><tbody>";

                            foreach (var item in levels)
                            {
                                textBody += "<tr><td>" + item.UserName + "</td><td align='right'>" + item.ExpectedHours + " Hrs</td><td align='right'>"
                                    + item.LeaveHours + "</td><td align='right'>" + item.PublicDays
                                    + "</td><td align='right'>" + (item.PlannedHours?.FormatForDisplay())
                                    + "</td><td align='right'>" + (item.ActualHours?.FormatForDisplay())
                                    + "</td><td align='right'>" + item.PercentageWorked?.FormatForDisplay();
                            }

                            textBody += "</tbody></table>";

                            mail.Body = textBody;
                            mail.IsBodyHtml = true;

                            SmtpServer.Port = Convert.ToInt32(SmtpPort);
                            SmtpServer.UseDefaultCredentials = false;
                            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                            SmtpServer.Credentials = new System.Net.NetworkCredential(SmtpEmailAddress, SmtpEmailPassword);
                            SmtpServer.EnableSsl = Convert.ToBoolean(EnableSsl);
                            SmtpServer.Timeout = 120000;
                            SmtpServer.Send(mail);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string ErrorMessage = e.Message;
            }
            return false;
        }
    }
}