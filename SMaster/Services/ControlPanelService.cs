﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SMaster.Helpers;
using SMaster.Models;
using SMaster.Models.Security;
using SMaster.Services;
using System;
using System.Linq;

namespace SMaster.Service.ControlPanel
{
    /// <summary>
    /// Provides data methods for the Control Panel
    /// </summary>
    public class ControlPanelService
    {
        private Repository database;

        public ControlPanelService()
        {
            database = new Repository();

            database.Configuration.AutoDetectChangesEnabled = false;
        }

        #region Users

        /// <summary>
        /// Gets all Users
        /// </summary>
        /// <returns></returns>
        public IQueryable<ApplicationUser> GetUsers()
        {
            return database.Users;
        }

        public IQueryable<IdentityRole> GetUserRoles()
        {
            return database.Roles;
        }

        /// <summary>
        /// Registers a new User
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="OtherName"></param>
        /// <param name="Email"></param>
        /// <param name="Phone"></param>
        /// <param name="CanCreateSprint"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public bool CreateUser(string UserName, string Password, string FirstName, string LastName, string OtherName
            , string Email, bool IsActive, string Phone, string UserRole,decimal? DailyRate, out string ErrorMessage)
        {
            ErrorMessage = null;

            try
            {
                using (Repository database = new Repository())
                {
                    var organisationSpecificUsername = UserName;
                    
                    //Validate Username
                    if (database.Users.Where(r => r.UserName == organisationSpecificUsername).Count() > 0)//Duplicate Username
                    {
                        throw new Exception("The UserName '" + organisationSpecificUsername + "' already exists!");
                    }

                    var newUser = new ApplicationUser
                    {
                        UserName = organisationSpecificUsername,
                        FirstName = FirstName,
                        LastName = LastName,
                        OtherName = OtherName,
                        DailyRate = DailyRate,
                        Email = Email,
                        PhoneNumber = Phone,
                        IsActive = IsActive
                    };

                    var userManager = SMasterUserManager.GetUserManager(database);

                    //Attempt to create User Account
                    var result = userManager.Create(newUser, Password);

                    if (!result.Succeeded)
                    {
                        var errorsFound = "<ul>";

                        foreach (var error in result.Errors)
                        {
                            errorsFound += "<li>" + error + "</li>";
                        }

                        errorsFound += "</ul>";

                        throw new Exception(errorsFound);
                    }
                    else//Bind User to Role
                    {
                        var user = userManager.FindByName(organisationSpecificUsername);
                        userManager.AddToRole(user.Id, UserRole);
                    }

                    database.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        /// <summary>
        /// Updates User Details
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="OtherName"></param>
        /// <param name="Email"></param>
        /// <param name="Phone"></param>
        /// <param name="OrganisationId"></param>
        /// <param name="NewUserRole"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public bool UpdateUser(string UserId, string FirstName, string LastName, string OtherName, string Email,string Initials, bool IsActive, string Phone,
            string NewUserRole, decimal? DailyRate, out string ErrorMessage)
        {
            ErrorMessage = null;

            try
            {
                using (Repository database = new Repository())
                {
                    var dbUser = database.Users.Find(UserId);

                    //Update fields
                    dbUser.FirstName = FirstName;
                    dbUser.LastName = LastName;
                    dbUser.OtherName = OtherName;
                    dbUser.Email = Email;
                    dbUser.PhoneNumber = Phone;
                    dbUser.Initials = Initials;
                    dbUser.DailyRate = DailyRate;
                    dbUser.IsActive = IsActive;
                    database.SaveChanges();

                    #region Update User Roles

                    bool currentRoleIsSimilarToNewRole = false;
                    var userManager = SMasterUserManager.GetUserManager(database);

                    if (dbUser.Roles.Count > 0)//Has existing roles
                    {
                        var userRoles = (from b in database.Users.Where(r => r.Id == UserId)
                                         from c in b.Roles
                                         join a in database.Roles on c.RoleId equals a.Id
                                         select a.Name).ToList();

                        //delete user from existing roles
                        foreach (var role in userRoles)
                        {
                            if (role.CompareTo(NewUserRole) == 0)//old role is same as new role
                            {
                                currentRoleIsSimilarToNewRole = true;
                                continue;
                            }

                            userManager.RemoveFromRole(UserId, role);
                        }
                    }

                    if (!currentRoleIsSimilarToNewRole && !string.IsNullOrEmpty(NewUserRole))//Attach to new role
                    { 
                        userManager.AddToRole(UserId, NewUserRole);
                    }

                    #endregion
                }
                return true;
            }
            catch (Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        public bool ResetUserPassword(string UserId,string NewPassword,out string ErrorMessage)
        {
            ErrorMessage = null;

            try
            {
                if (string.IsNullOrEmpty(NewPassword))
                {
                    throw new Exception("The New Password is required!");
                }

                using(Repository database=new Repository())
                {
                    var userManager = SMasterUserManager.GetUserManager(database);

                    var resetToken = userManager.GeneratePasswordResetToken(UserId);

                    var result=userManager.ResetPassword(UserId, resetToken, NewPassword);

                    if(!result.Succeeded)
                    {
                        string Errors = "<ul>";

                        foreach(var error in result.Errors)
                        {
                            Errors += "<li>"+error+"</li>";
                        }

                        Errors += "</ul>";
                        throw new Exception(Errors);
                    }
                }

                return true;
            }
            catch(Exception e)
            {
                ErrorMessage = UtilityService.ExtractInnerExceptionMessage(e);
            }

            return false;
        }

        #endregion
    }
}
