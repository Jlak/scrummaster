﻿using SMaster.Attributes.Filters;
using System.Web;
using System.Web.Mvc;

namespace SMaster
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new AuthorizeAttribute());
            filters.Add(new ExtractSessionInfoAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
