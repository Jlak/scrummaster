﻿using SMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace SMaster.Helpers
{
    /// <summary>
    /// Extends HtmlHelper to add custom control rendering
    /// </summary>
    public static class FormControlHelper
    {
        /// <summary>
        /// Renders a Textbox in a div with a label in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString TextBoxWithLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, object HtmlAttributes = null)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.LabelFor(Expression), Html.TextBoxFor(Expression, HtmlAttributes)
                , Html.ValidationMessageFor(Expression));
        }


        /// <summary>
        /// Renders a Textbox in a div with a label in a div with the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString TextBoxWithoutLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, object HtmlAttributes = null)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithoutLabel", null, Html.TextBoxFor(Expression, HtmlAttributes)
                , Html.ValidationMessageFor(Expression));
        }

        /// <summary>
        /// Renders a Dropdown List with a label in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="Expression"></param>
        /// <param name="SelectList"></param>
        /// <param name="HtmlAttributes"></param>
        /// <param name="OptionalLabel"></param>
        /// <returns></returns>
        public static MvcHtmlString DropDownListWithLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression
            , SelectList SelectList, object HtmlAttributes, string OptionalLabel)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.LabelFor(Expression),
                Html.DropDownListFor(Expression, SelectList, OptionalLabel, HtmlAttributes), Html.ValidationMessageFor(Expression));
        }

        /// <summary>
        /// Renders a Dropdown List with a label in a div with the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="Expression"></param>
        /// <param name="SelectList"></param>
        /// <param name="HtmlAttributes"></param>
        /// <param name="OptionalLabel"></param>
        /// <returns></returns>
        public static MvcHtmlString DropDownListWithoutLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression
            , SelectList SelectList, object HtmlAttributes, string OptionalLabel)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithoutLabel", null,
                Html.DropDownListFor(Expression, SelectList, OptionalLabel, HtmlAttributes), Html.ValidationMessageFor(Expression));
        }

        /// <summary>
        /// Renders a TextArea in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString TextAreaWithLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, object HtmlAttributes = null)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.LabelFor(Expression), Html.TextAreaFor(Expression, HtmlAttributes)
                , Html.ValidationMessageFor(Expression));
        }

        /// <summary>
        /// Renders a TextArea in a div with the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString TextAreaWithoutLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, object HtmlAttributes = null)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithoutLabel", null, Html.TextAreaFor(Expression, HtmlAttributes)
                , Html.ValidationMessageFor(Expression));
        }
        /// <summary>
        /// Renders a Password in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString PasswordWithLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, object HtmlAttributes = null)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.LabelFor(Expression), Html.PasswordFor(Expression, HtmlAttributes)
                , Html.ValidationMessageFor(Expression));
        }
        /// <summary>
        /// Renders a Date Picker in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="AllowFutureDates"></param>
        /// <returns></returns>
        public static MvcHtmlString DatePickerWithLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, bool AllowFutureDates = false, bool ShowTime = false)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.LabelFor(Expression)
                , Html.TextBoxFor(Expression, ShowTime ? "{0:yyyy-MM-dd HH:mm}" : "{0:yyyy-MM-dd}", new { @class = "datepicker " + (AllowFutureDates ? "future" : "present") + (ShowTime ? " show-time" : "") })
                , Html.ValidationMessageFor(Expression));
        }
        /// <summary>
        /// Renders a Date Picker for the future starting today in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="AllowFutureDates"></param>
        /// <returns></returns>
        public static MvcHtmlString FutureDatePickerWithLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, bool AllowFutureDates = false, bool ShowTime = false)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.LabelFor(Expression)
                , Html.TextBoxFor(Expression, ShowTime ? "{0:yyyy-MM-dd HH:mm}" : "{0:yyyy-MM-dd}", new { @class = "datepicker no-past" + (AllowFutureDates ? "future" : "present") + (ShowTime ? " show-time" : "") })
                , Html.ValidationMessageFor(Expression));
        }
        /// <summary>
        /// Renders a Date Picker for the future starting today in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="AllowFutureDates"></param>
        /// <returns></returns>
        public static MvcHtmlString FutureDatePickerWithOutLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithoutLabel", null
                , Html.TextBoxFor(Expression, "{0:yyyy-MM-dd}", new { @class = "datepicker no-past", @readonly = "readonly" })
                , Html.ValidationMessageFor(Expression));
        }
        /// <summary>
        /// Renders a Date Picker in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="AllowFutureDates"></param>
        /// <returns></returns>
        public static MvcHtmlString DatePickerWithoutLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression, bool AllowFutureDates = false, bool ShowTime = false)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithoutLabel", null
                , Html.TextBoxFor(Expression, ShowTime ? "{0:yyyy-MM-dd HH:mm}" : "{0:yyyy-MM-dd}", new { @class = "datepicker " + (AllowFutureDates ? "future" : "present") + (ShowTime ? " show-time" : "") })
                , Html.ValidationMessageFor(Expression));
        }


        /// <summary>
        /// Renders a Password in a form-group div with a label, the control and validation message
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="Html"></param>
        /// <param name="Label"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString PasswordWithLabel<TModel>(this HtmlHelper<TModel> Html, string Label, string ControlId, object HtmlAttributes = null)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.Label(Label), Html.Password(ControlId, null, HtmlAttributes)
                , Html.ValidationMessage(ControlId));
        }

        /// <summary>
        /// Displays a value in a div with a label
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString DisplayWithLabelFor<TModel, TProperty>(this HtmlHelper<TModel> Html, Expression<Func<TModel, TProperty>> Expression)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.LabelFor(Expression), Html.DisplayFor(Expression)
                , null);
        }

        /// <summary>
        /// Displays a value in a div with a label
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString DisplayWithLabel<TModel>(this HtmlHelper<TModel> Html, string Label, string Value)
        {
            return RenderPartial(Html, "Partials/Controls/_PartialControlWithLabel", Html.Label(Label), Value
                , null);
        }

        /// <summary>
        /// Renders a submit button
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="Id">Html Id</param>
        /// <param name="Title"></param>
        /// <param name="ExtraCSS">Extra CSS classes</param>
        /// <returns></returns>
        public static HtmlString SubmitButton(this HtmlHelper Html, string Title, string ExtraCSS = null, string Id = null)
        {
            return (HtmlString)Html.Raw("<input type=\"submit\" " + (Id != null ? "id=\"" + Id + "\"" : "") + " value=\"" + Title + "\" class=\"btn btn-primary " + (ExtraCSS ?? "") + "\"/>");
        }

        /// <summary>
        /// Renders a Hidden Fields especially useful for Id columns in partials
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <param name="HtmlAttributes"></param>
        /// <returns></returns>
        public static HtmlString HiddenField<TModel>(this HtmlHelper<TModel> Html, string Name, object Value)
        {
            return (HtmlString)Html.Raw("<input type=\"hidden\" name=\"" + Name + "\" id=\"" + Name + "\" value=\"" + Value + "\" />");
        }

        private static MvcHtmlString RenderPartial(HtmlHelper Html, string PartialUrl, object Label, object Control, object Validation)
        {
            ViewDataDictionary dictionary = new ViewDataDictionary();

            dictionary.Add("Label", Label);
            dictionary.Add("Control", Control);
            dictionary.Add("Validation", Validation);

            return PartialExtensions.Partial(Html, PartialUrl, dictionary);
        }

        /// <summary>
        /// Renders an Error Message
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="Message">The Error Message</param>
        /// <returns></returns>
        public static MvcHtmlString ErrorMessage(this HtmlHelper Html, string Message)
        {
            return Html.Partial("Partials/_PartialErrorMessage", Message);
        }

        /// <summary>
        /// Renders an Error Message of IErrorMessage models
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="Model">The IErrorMessage object</param>
        /// <returns></returns>
        public static MvcHtmlString ErrorMessageFor(this HtmlHelper Html, IErrorMessage Model)
        {
            string Message = Model.ErrorMessage;

            return Html.ErrorMessage(Message ?? "");
        }

        /// <summary>
        /// Renders a Success Message
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="Message">The Success Message</param>
        /// <returns></returns>
        public static MvcHtmlString SuccessMessageFor(this HtmlHelper Html, string Message)
        {
            return Html.Partial("Partials/_PartialSuccessMessage", Message);
        }
    }
}
