﻿using Microsoft.AspNet.Identity;
using SMaster.Enums;
using SMaster.Interfaces;
using SMaster.Models;
using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SMaster.Helpers
{
    /// <summary>
    /// Provides acces to Lookups
    /// </summary>
    public static class LookupsHelper
    {
        /// <summary>
        /// Queries for Db Items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="WhereClause">can be null</param>
        /// <param name="SelectClause">Required</param>
        /// <param name="OrderByClause">can be null</param>
        /// <returns></returns>
        public static List<object> GetDbItems<T>(Expression<Func<T, bool>> WhereClause, Expression<Func<T, object>> SelectClause, Expression<Func<T, object>> OrderByClause, Repository db = null) where T : class
        {
            using (var database = db ?? Repository.Create(false))
            {
                var query = database.Set<T>().Select(r => r);

                if (WhereClause != null)
                {
                    query = query.Where(WhereClause);
                }

                if (OrderByClause != null)
                {
                    query = query.OrderBy(OrderByClause);
                }

                return query.Select(SelectClause).ToList();
            }
        }

        public static Dictionary<string, object> GetEnumValues(Type T)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            foreach (var value in Enum.GetValues(T))
            {
                result.Add(Enum.GetName(T, value).Replace("__", ": ").Replace("_", " "), value);
            }

            return result;
        }

        public static Dictionary<string, string> GetProjectManagers()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            using (var database = Repository.Create(false))
            {
                var userManager = SMasterUserManager.GetUserManager(database);

                foreach (var item in database.Users.ToList())
                {
                    if (userManager.IsInRole(item.Id, SMasterUserManager.PROJECT_MANAGER))
                    {
                        result.Add(item.FirstName +" "+ item.LastName +" "+ item.OtherName , item.Id);
                    }
                }
            }

            return result;
        }

        public static Dictionary<string, int> GetProjects()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            using (var database = Repository.Create(false))
            {
                foreach (var item in database.Projects.OrderBy(r => r.Name).ToList())
                {
                    result.Add(item.Name, item.Id);
                }
            }

            return result;
        }



        public static Dictionary<string, string> GetUserDetails()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            using (var database = Repository.Create(false))
            {
                foreach (var item in database.Users.Where(r=>r.IsActive !=false).OrderBy(r => r.FirstName).ToList())
                {
                    result.Add(item.FirstName +" "+ item.LastName, item.Id);
                }
            }

            return result;
        }

        public static int GetPending()
        {
            var reportingDate = DateTime.Today.AddDays(-1);
            using (var database = Repository.Create(false))
            {
                var UserId = HttpContext.Current.User.Identity.GetUserId();
                return database.SprintTaskHistories.Where(r => r.Assignee.SprintTask.ExtraActivity == null)
                    .Where(r => r.Assignee.UserId == UserId && r.Status < ActivityStatus.Done
                    && r.ActivityDate < reportingDate && r.DateToComplete == null).Count();
            }
        }

        public static Dictionary<string, int> GetProjectClients(int ProjectId)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            using (var database = Repository.Create(false))
            {
                foreach (var item in database.Clients.Where(r=>r.ProjectId == ProjectId).OrderBy(r => r.Name).ToList())
                {
                    result.Add(item.Name, item.Id);
                }
            }

            return result;
        }
        public static Dictionary<string, int> GetProjectsWithSprints()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            using (var database = Repository.Create(false))
            {
                foreach (var item in database.Projects.OrderBy(r => r.Name).ToList())
                {
                    var sprints = database.SprintPlans.Where(r => r.Tasks.Count() > 0 && r.ProjectId == item.Id && r.Status == ActivityStatus.Ongoing).FirstOrDefault();
                    if (sprints != null)
                    {
                        result.Add(item.Name, item.Id);
                    }
                }
            }

            return result;
        }
        public static List<ApplicationUser> GetApplicationUsers()
        {
            using (var database = Repository.Create(false))
            {
                return database.Users.Where(r=>r.UserName != "Admin" && r.IsActive).OrderBy(r => new
                {
                    r.FirstName,
                    r.LastName,
                    r.OtherName
                }).ToList();
            }
        }

        public static Dictionary<string, string> GetUsers()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            using (var database = Repository.Create(false))
            {
                var users = database.Users.Distinct().ToList();
                foreach (var item in users)
                {
                    if (users != null)
                    {
                        result.Add(item.FirstName +" "+ item.LastName +" "+ item.OtherName, item.Id);
                    }
                }
            }
            return result;
        }

        public static Dictionary<int, int> GetYears()
        {
            Dictionary<int, int> result = new Dictionary<int, int>();

            using (var database = Repository.Create(false))
            {
                var years = database.SprintTaskHistories.Select(r=>r.ActivityDate.Value.Year).Distinct().ToList();
                foreach (var item in years)
                {
                    if (years != null)
                    {
                        result.Add(item, item);
                    }
                }
            }
            return result;
        }

        public static Dictionary<string, int> GetMonths()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            for(int i = 1; i <= 12; i++)
            {
                result.Add(new DateTime(2012, i, 1).ToString("MMMM"), i);
            }

            return result;
        }

        public static Dictionary<string, string> GetAdministratorRoles()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            result.Add(SMasterUserManager.SuperRole, SMasterUserManager.SuperRole);
            result.Add(SMasterUserManager.DEVELOPER, SMasterUserManager.DEVELOPER);
            result.Add(SMasterUserManager.REGULAR_USER, SMasterUserManager.REGULAR_USER);
            result.Add(SMasterUserManager.SCRUM_MASTER, SMasterUserManager.SCRUM_MASTER);
            result.Add(SMasterUserManager.PROJECT_MANAGER, SMasterUserManager.PROJECT_MANAGER);
            result.Add(SMasterUserManager.QUALITY_ASSURANCE, SMasterUserManager.QUALITY_ASSURANCE);
            return result;
        }
    }
}