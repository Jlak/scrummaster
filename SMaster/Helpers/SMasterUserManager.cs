﻿using SMaster.Models;
using SMaster.Models.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FSDU.Helpers;

namespace SMaster.Helpers
{
    /// <summary>
    /// Used to manage system users
    /// </summary>
    public static class SMasterUserManager
    {
        /// <summary>
        /// The system administrator
        /// </summary>
        public const string SuperUser = "Admin";

        /// <summary>
        /// The role with full access
        /// </summary>
        public const string SuperRole = "Administrator";
        public const string REGULAR_USER = "Regular User";
        public const string SessionKey = "UserInfo";
        public const string SCRUM_MASTER = "Scrum Master";
        public const string DEVELOPER = "Developer";
        public const string QUALITY_ASSURANCE = "Quality Assurance";
        public const string PROJECT_MANAGER = "Project Manager";
        /// <summary>
        /// Returns the roles used to manage access to the system i.e. doesn't include Administrator
        /// </summary>
        /// <returns></returns>
        public static string[] GetSystemUserRoles()
        {
            return new[] { SuperRole, SCRUM_MASTER, REGULAR_USER , DEVELOPER , QUALITY_ASSURANCE, PROJECT_MANAGER };
        }
        /// <summary>
        /// Gets the User Manager for managing users
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        public static UserManager<ApplicationUser> GetUserManager(Repository database)
        {
            var userStore = new UserStore<ApplicationUser>(database);
            var userManager = new UserManager<ApplicationUser>(userStore);

            ConfigureUserManager(userManager);

            return userManager;
        }

        /// <summary>
        /// Configures a User Manager
        /// </summary>
        /// <param name="userManager"></param>
        public static void ConfigureUserManager(UserManager<ApplicationUser> userManager)
        {
            // Configure validation logic for usernames
            userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            userManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            userManager.UserLockoutEnabledByDefault = true;
            userManager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            userManager.MaxFailedAccessAttemptsBeforeLockout = 5;

            var provider = new DpapiDataProtectionProvider("SMaster");

            userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("ResetPassword"));
        }

        /// <summary>
        /// Initializes the Super User and Super Role for the system
        /// </summary>
        public static void Initialize(Repository database)
        {
            var userManager = GetUserManager(database);

            ApplicationUser defaultUser = userManager.FindByName(SuperUser);

            if (defaultUser == null)//Super User missing
            {
                var result = userManager.Create(new ApplicationUser
                {
                    UserName = SuperUser,
                    Email = "support@dcareug.com"
                }, "administrator");

                if (result.Succeeded)
                {
                    defaultUser = userManager.FindByName(SuperUser);
                }
                else
                {
                    throw new Exception(ExtractErrors(result.Errors));
                }
            }

            if (database.Roles.Where(r => r.Name == SuperRole).Count() == 0)//Super Role Missing
            {
                database.Roles.Add(new IdentityRole(SuperRole));
                database.SaveChanges();
            }

            if (database.Roles.Where(r => r.Name == SCRUM_MASTER).Count() == 0)//SCRUM MASTER Role Missing
            {
                database.Roles.Add(new IdentityRole(SCRUM_MASTER));
                database.SaveChanges();
            }

            if (database.Roles.Where(r => r.Name == REGULAR_USER).Count() == 0)//REGULAR USER Role Missing
            {
                database.Roles.Add(new IdentityRole(REGULAR_USER));
                database.SaveChanges();
            }

            if (database.Roles.Where(r => r.Name == DEVELOPER).Count() == 0)//DEVELOPER Role Missing
            {
                database.Roles.Add(new IdentityRole(DEVELOPER));
                database.SaveChanges();
            }

            if (database.Roles.Where(r => r.Name == QUALITY_ASSURANCE).Count() == 0)//QUALITY ASSURANCE Role Missing
            {
                database.Roles.Add(new IdentityRole(QUALITY_ASSURANCE));
                database.SaveChanges();
            }

            if (database.Roles.Where(r => r.Name == PROJECT_MANAGER).Count() == 0)//PROJECT MANAGER Role Missing
            {
                database.Roles.Add(new IdentityRole(PROJECT_MANAGER));
                database.SaveChanges();
            }

            if (!userManager.IsInRole(defaultUser.Id, SuperRole))//User not in Role
            {
                userManager.AddToRole(defaultUser.Id, SuperRole);
            }
        }

        private static string ExtractErrors(IEnumerable<string> Errors)
        {
            var MergedErrors = "";

            foreach (var error in Errors)
            {
                if (MergedErrors.Length > 1)
                {
                    MergedErrors += ", ";
                }
                MergedErrors += error;
            }

            return MergedErrors;
        }
        /// <summary>
        /// Extracts the logged in user's ApplicationUser object and saves it for the current session
        /// </summary>
        /// <param name="UserName"></param>
        public static void SaveUserSessionInfo(string UserName)
        {
            using (var database = Repository.Create(false))
            {
                HttpContext.Current.Items.Add(SessionKey, database.Users.Where(r => r.UserName == UserName)
                    .Single());
            }
        }

        /// <summary>
        /// Returns the currently logged in ApplicationUser object
        /// </summary>
        /// <returns></returns>
        public static ApplicationUser GetUserSessionInfo()
        {
            return (ApplicationUser)HttpContext.Current.Items[SessionKey];
        }

        public static List<SMasterSecurityManagerInfo> GetCurrentOrganisationUsers(string FilterUserId = null)
        {
            using (var configDatabase = new Repository())
            {
                var currentOrganisationId = SessionUserInfo.GetCurrentUserInfo().AppUser.Id;

                var query = configDatabase.Users.Where(r => r.Id == currentOrganisationId);

                if (!string.IsNullOrEmpty(FilterUserId))
                {
                    query = query.Where(r => r.Id == FilterUserId);
                }

                return query
                    .OrderBy(r => r.UserName)
                    .Select(r => new SMasterSecurityManagerInfo
                    {
                        FirstName = r.FirstName,
                        Id = r.Id,
                        LastName = r.LastName,
                        OtherName = r.OtherName,
                        UserName = r.UserName
                    }).ToList();
            }
        }
    }
}
