﻿using SMaster.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Helpers
{
    public static class SMasterSecurityManager
    {
        public static List<SMasterSecurityManagerInfo> GetCurrentOrganisationUsers(string FilterUserId = null)
        {
            using (var database = new Repository())
            {

                var query = database.Users;

                return query
                    .OrderBy(r => r.UserName)
                    .Select(r => new SMasterSecurityManagerInfo
                    {
                        FirstName = r.FirstName,
                        Id = r.Id,
                        LastName = r.LastName,
                        OtherName = r.OtherName,
                        UserName = r.UserName
                    }).ToList();
            }
        }

        #region Action Protection
        

        #endregion
    }

    public class SMasterSecurityManagerInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Key
        {
            get
            {
                return string.Format("{0} [{1} {2} {3}]", UserName, FirstName, LastName, OtherName);
            }
        }

        public string Value
        {
            get
            {
                return Id;
            }
        }

        public bool UserIdMatches(string UserId)
        {
            return string.Compare(UserId, Id, true) == 0;
        }
    }
}