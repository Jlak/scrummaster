﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SMaster.Helpers
{

    public static class PagedListHelper
    {

        /// <summary>
        /// Loads the RowNumber field for PagedList Items
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="PagedListItems"></param>
        public static void LoadRowNumbers(this HtmlHelper Html, IPagedList<IPagedListItem> PagedListItems)
        {
            var rowNumber = (PagedListItems.PageNumber - 1) * PagedListItems.PageSize;

            foreach (var item in PagedListItems)
            {
                item.RowNumber = ++rowNumber;
            }
        }

        /// <summary>
        /// Returns the start Item No for a given PagedList
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="PagedList"></param>
        /// <returns></returns>
        public static int GetPagedListStartItemNo(this HtmlHelper Html, IPagedList PagedList)
        {
            return ((PagedList.PageNumber - 1) * PagedList.PageSize) + 1;
        }


        public interface IPagedListItem
        {
            int RowNumber { get; set; }
        }
    }
}