﻿using SMaster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SMaster.Helpers
{
    /// <summary>
    /// A Generic Lookup Helper
    /// </summary>
    public static class GenericLookupHelper
    {
        /// <summary>
        /// Creates a Lookup for any given type in any given type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="WhereClause">Optional</param>
        /// <param name="SelectClause">Optional</param>
        /// <param name="OrderByClause">Optional</param>
        /// <returns></returns>
        public static List<Object> GetDbSetLookup<T>(Expression<Func<T, bool>> WhereClause = null, Expression<Func<T, Object>> SelectClause = null,
            Expression<Func<T, Object>> OrderByClause = null) where T : class
        {
            using (Repository database = new Repository())
            {
                var queryLevel1 = database.Set<T>().Select(r => r);

                if (WhereClause != null)
                {
                    queryLevel1 = queryLevel1.Where(WhereClause);
                }
                if (OrderByClause != null)
                {
                    queryLevel1 = queryLevel1.OrderBy(OrderByClause);
                }

                return (SelectClause != null ? (queryLevel1.Select(SelectClause)) : queryLevel1).ToList();
            }
        }
        /// <summary>
        /// Queries for Db Items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="WhereClause">can be null</param>
        /// <param name="SelectClause">Required</param>
        /// <param name="OrderByClause">can be null</param>
        /// <returns></returns>
        public static List<object> GetDbItems<T>(Expression<Func<T, bool>> WhereClause, Expression<Func<T, object>> SelectClause,
            Expression<Func<T, object>> OrderByClause, Repository db = null, bool AscendingOrder = true) where T : class
        {
            using (Repository database = db ?? new Repository())
            {
                var query = database.Set<T>().Select(r => r);

                if (WhereClause != null)
                {
                    query = query.Where(WhereClause);
                }

                if (OrderByClause != null)
                {
                    if (AscendingOrder)
                    {
                        query = query.OrderBy(OrderByClause);
                    }
                    else
                    {
                        query = query.OrderByDescending(OrderByClause);
                    }
                }

                return query.Select(SelectClause).ToList();
            }
        }


        /// <summary>
        /// Exports the contents of a partial view to excel.
        /// </summary>
        /// <param name="Response">The HttpResponseBase to override.</param>
        /// <param name="partial">The PartialViewResult to export.</param>
        /// <param name="Filename">The name of the file generated.</param>
        /// <returns></returns>
        public static ActionResult ExportPartialToExcel(HttpResponseBase Response, PartialViewResult partial, string Filename)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename.Replace(" ", "_") + ".xls");
            Response.ContentType = "application/ms-excel";

            return partial;
        }

        public static string FormatForDisplay(this decimal model)
        {
            return model.ToString("#,##0.##");
        }

        public static string FormatForDisplay(this DateTime Date, bool WithTime = false)
        {
            return Date.ToString(WithTime ? "dd MMM yyyy HH:mm" : "dd MMM yyyy");
        }

        public static string FormatForDisplay(this DateTime? Date, bool WithTime = false)
        {
            return Date?.FormatForDisplay(WithTime);
        }
        public static int GetNumberOfWorkingDays(DateTime start, DateTime stop)
        {
            TimeSpan interval = stop - start;

            int totalWeek = interval.Days / 7;
            int totalWorkingDays = 5 * totalWeek;

            int remainingDays = interval.Days % 7;

            for (int i = 0; i <= remainingDays; i++)
            {
                DayOfWeek test = (DayOfWeek)(((int)start.DayOfWeek + i) % 7);
                if (test >= DayOfWeek.Monday && test <= DayOfWeek.Friday)
                    totalWorkingDays++;
            }

            return totalWorkingDays;
        }

        public static IEnumerable<object> GetMonthsOfYear()
        {
            for (var i = 1; i <= 12; i++)
            {
                yield return new
                {
                    Key = GetMonthName(i),
                    Value = i
                };
            }
        }

        public static string GetMonthName(int MonthNo)
        {
            return new DateTime(2015, MonthNo, 1).ToString("MMMM");
        }
        public static IEnumerable<int> GetYears()
        {
            for (var i = DateTime.Today.Year + 1; i >= 2016; i--)
            {
                yield return i;
            }
        }
    }
}