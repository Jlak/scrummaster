﻿using SMaster.Helpers;
using SMaster.Models;
using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FSDU.Helpers
{
    public class SessionUserInfo
    {
        #region Private Fields
        
        private string _username;
        private ApplicationUser _appuser;
        private bool IsASystemAdmin;

        #endregion

        #region Properties

        public ApplicationUser AppUser
        {
            get
            {
                return _appuser;
            }
        }

        public string UserName
        {
            get
            {
                return _username;
            }
        }

        #endregion

        #region Constants

        private const string Parameter_UserName = "UserName";
        private const string SessionKey = "SessionUser";

        #endregion

        public SessionUserInfo()
        {
        }

        private static void SaveUserSessionInfo(string UserName, bool LoggedInFromMobileDevice = false)
        {
            var userInfo = new SessionUserInfo();

            using (var database = Repository.Create(false))
            {
                var dbUser = database.Users.Where(r => r.UserName == UserName).Single();

                userInfo._username = UserName;
                userInfo._appuser = dbUser;

                userInfo.IsASystemAdmin = HttpContext.Current.User.IsInRole(SMasterUserManager.SuperRole);
            }

            HttpContext.Current.Items.Add(SessionKey, userInfo);
        }

        public static void ExtractMVCUserInfo()
        {
            SaveUserSessionInfo(HttpContext.Current.User.Identity.Name);
        }

        public static SessionUserInfo GetCurrentUserInfo()
        {
            return (SessionUserInfo)HttpContext.Current.Items[SessionKey];
        }
    }
}