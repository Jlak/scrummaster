﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Helpers
{
    /// <summary>
    /// Provides additional functionality for rendering Views
    /// </summary>
    public static class ViewHelper
    {
        /// <summary>
        /// Returns an Enum's values as a dictionary&lt;Key,Value&gt;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Dictionary<string, object> GetEnumValues<T>()
        {
            Dictionary<string, object> results = new Dictionary<string, object>();

            var EnumType = typeof(T);

            foreach (var EnumValue in Enum.GetValues(EnumType))
            {
                results.Add(Enum.GetName(EnumType, EnumValue), EnumValue);
            }

            return results;
        }

        /// <summary>
        /// Toggles value green/crimson colors depending on whether or not ActualValue==GreenValue
        /// </summary>
        /// <param name="GreenValue">The value that defaults to Green</param>
        /// <param name="ActualValue">The Actual Value</param>
        /// <returns></returns>
        public static string ToggleGreenOrCrimsonValue(object GreenValue, object ActualValue,string DisplayText)
        {
            var color = ((int)ActualValue == (int)GreenValue ? "green" : "crimson");

            return "<span style='color:" + color + "'>" + DisplayText??ActualValue + "</span>";
        }


        /// <summary>
        /// Breaks a SearchTerm into a collection of terms i.e. splits by space.
        /// </summary>
        /// <param name="SearchTerm"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetSearchTerms(string SearchTerm)
        {
            if (string.IsNullOrEmpty(SearchTerm))
            {
                return new List<string>();
            }
            return SearchTerm.Split(' ');
        }
        public static string GetEnumName(Enum Value)
        {
            return Enum.GetName(Value.GetType(), Value);
        }

        public static int GetEnumAsInt(this Enum Value)
        {
            return Convert.ToInt32(Value);
        }
    }
}
