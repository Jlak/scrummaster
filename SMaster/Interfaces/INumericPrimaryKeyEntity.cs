﻿namespace SMaster.Interfaces
{
    /// <summary>
    /// A DB Entity with an Integer Primary Key with the name Id
    /// </summary>
    public interface INumericPrimaryKeyEntity
    {
        int Id { get; set; }
    }
}
