﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Interfaces
{
    public interface IErrorMessage
    {
        string ErrorMessage { get; set; }
    }
}
