﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Interfaces
{ 
    /// <summary>
    /// A Custom Lookup
    /// </summary>
    public interface ICustomLookups : IAuditTrail, INumericPrimaryKeyEntity, IErrorMessage
    {
    }
}
