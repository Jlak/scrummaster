﻿using SMaster.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMaster.Interfaces
{
    /// <summary>
    /// Implements an Audit Trail
    /// </summary>
    public interface IAuditTrail
    {
        //Creation Tracking
        DateTime? CreatedOn { get; set; }
        string CreatedBy { get; set; }

        //Last Modification Tracking
        DateTime? LastModifiedOn { get; set; }
        string LastModifiedBy { get; set; }
    }
}
