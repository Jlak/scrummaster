﻿namespace SMaster.Interfaces
{
    /// <summary>
    /// Represents an Admin Unit i.e. a lookup
    /// </summary>
    public interface IAdminUnit
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
